import 'package:flutter/material.dart';

class ConfirmDialog extends StatefulWidget {
  final String message;
  final String? text;
  final Widget? content;
  final String trueButtonText;
  final String falseButtonText;
  const ConfirmDialog({
    Key? key,
    required this.message,
    this.text,
    this.content,
    required this.trueButtonText,
    required this.falseButtonText,
  }) : super(key: key);

  @override
  State<ConfirmDialog> createState() => _ConfirmDialogState();

  static Future<bool> show(
      BuildContext context,
      String message,
      {
        String trueButtonText = "Sim",
        String falseButtonText = "Não",
        String? text,
        Widget? content,
      }
      ) async {
    bool? response = await showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => ConfirmDialog(
            message: message,
            text: text,
            content: content,
            trueButtonText: trueButtonText,
            falseButtonText: falseButtonText
        )
    );
    return response ?? false;
  }
}

class _ConfirmDialogState extends State<ConfirmDialog> {

  @override
  Widget build(BuildContext context) {
    Widget? content = widget.content;
    if (content == null && widget.text != null){
      content = Text(
        widget.text!,
        textAlign: TextAlign.center,
        style: const TextStyle(
          fontSize: 16,
        ),
      );
    }
    return WillPopScope(
      onWillPop: () async => false,
      child: AlertDialog(
        title: Text(widget.message, textAlign: TextAlign.center,),
        content: content,
        titlePadding: const EdgeInsets.only(top: 20, bottom: 0, left: 20, right: 20),
        actions: <Widget>[
          TextButton(
            child: Text(widget.falseButtonText),
            onPressed: () {
              Navigator.of(context).pop(false);
            },
          ),
          TextButton(
            child: Text(widget.trueButtonText),
            onPressed: () {
              Navigator.of(context).pop(true);
            },
          ),
        ],
      ),
    );
  }
}
