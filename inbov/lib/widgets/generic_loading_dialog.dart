import 'package:flutter/material.dart';

// ignore: must_be_immutable
class LoadingDialog extends StatefulWidget {
  BuildContext context;
  String? message;
  LoadingDialog({
    Key? key,
    this.message,
    required this.context,
  }) : super(key: key);

  @override
  _LoadingDialogState createState() => _LoadingDialogState();

  static void show(
      BuildContext context,
      {String? message}) async {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) => LoadingDialog(context: context, message: message));
  }
}

class _LoadingDialogState extends State<LoadingDialog> {

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: AlertDialog(
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const CircularProgressIndicator(),
            const SizedBox(height: 10),
            Text(widget.message != null ? widget.message! : "Salvando..."),
          ],
        ),
      ),
    );
  }
}
