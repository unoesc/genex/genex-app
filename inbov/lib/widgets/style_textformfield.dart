import '../configs.dart';
import 'package:flutter/material.dart';

const double defaultBorderWidth = 1.5;
const double defaultBorderRadius = 8.0;

OutlineInputBorder customInputBorder() {
  return const OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(defaultBorderRadius)),
    borderSide: BorderSide(
      color: primary,
      width: defaultBorderWidth,
    )
  );
}

OutlineInputBorder customFocusBorder() {
  return const OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(defaultBorderRadius)),
    borderSide: BorderSide(
      color: primary,
      width: defaultBorderWidth,
    )
  );
}
