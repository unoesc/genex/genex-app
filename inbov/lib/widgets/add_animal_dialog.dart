import 'package:flutter/material.dart';
import 'package:inbov/configs.dart';
import 'package:inbov/utils/validators.dart';
import 'package:inbov/widgets/response_confirmation.dart';
import 'package:inbov/widgets/text_field.dart';
//ignore: library_prefixes
import '../../http_requests/http_animal.dart' as dioAnimal;
import '../models/area_model.dart';


class AddAnimal extends StatefulWidget {
  final AreaGet area;
  const AddAnimal({
    Key? key,
    required this.area,
  }) : super(key: key);
  @override
  State<AddAnimal> createState() => _AddAnimalState();


  static Future<void> show(BuildContext context, AreaGet area) async {
    await showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AddAnimal(area: area);
      },
    );
  }
}

class _AddAnimalState extends State<AddAnimal> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _idController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  bool requestLoading = false;


  Future<void> submitFunction() async {
    setState(() {
      requestLoading = true;
    });
    try{
      Map<String, dynamic> data = {
        "name": _nameController.text.isNotEmpty ? _nameController.text : null,
        "id": _idController.text,
        "area_id": widget.area.id,
      };
      await dioAnimal.createAnimal(data);
      //ignore: use_build_context_synchronously
      Navigator.of(context).pop();
    }catch(e, stacktrace){
      print(e);
      print(stacktrace);
      if (e == 404){
        ResponseConfirmation.show(context, "Area não encontrada", 404);
      }
      else if (e == 409){
        ResponseConfirmation.show(context, "O ID informado já possui cadastro", 409);
      }
      else{
        ResponseConfirmation.show(context, "Ocorreu um erro interno, tente mais tarde", 500);
      }
    }finally{
      setState(() {
        requestLoading = false;
      });
    }
  }


  Widget _entryField(String title, TextEditingController controller, Function(String)? validate, TextInputType inputType, {String? mask}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 10),
        TextFieldPage(
          controller: controller,
          loading: requestLoading,
          obscure: false,
          text: title,
          validate: validate,
          focus: false,
          outlined: true,
          typeInput: inputType,
          mask: mask,
        ),
      ],
    );
  }


  Widget submitButton() {
    return InkWell(
      onTap: () async {
        if (_formKey.currentState!.validate()) {
          await submitFunction();
        }
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: const LinearGradient(
            colors: [
              Color(0xFF4caf50),
              Color(0xFF8bc34a),
            ],
          ),
        ),
        child: const Text(
          'Salvar',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }





  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: AlertDialog(
        contentPadding: const EdgeInsets.all(20),
        content: SizedBox(
          width: 400,
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child:
              Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    children: [
                      const Text(
                          "Adicionar Animal",
                          style:
                          TextStyle(
                              fontSize: 20,
                              color: secondary,
                              fontWeight: FontWeight.w800
                          )
                      ),
                      const Spacer(),
                      IconButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: const Icon(Icons.close)
                      ),
                    ],
                  ),
                  _entryField("Brinco/identificador *", _idController, requiredFieldValidator, TextInputType.name),
                  _entryField("Nome", _nameController, null, TextInputType.name),
                  const SizedBox(height: 10),
                  SizedBox(
                    width: double.infinity,
                    child: requestLoading
                        ? const Center(
                          child: CircularProgressIndicator(),
                        )
                        : submitButton()
                  )
                ],
              )
          )
        )
      ),
    );
  }

}
