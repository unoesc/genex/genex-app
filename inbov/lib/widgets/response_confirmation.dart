import 'package:flutter/material.dart';

class ResponseConfirmation extends StatefulWidget {
  final String title;
  final int error;
  final int multiplePop;
  const ResponseConfirmation(
      {Key? key, required this.title, required this.error, required this.multiplePop})
      : super(key: key);

  @override
  _ResponseConfirmationState createState() => _ResponseConfirmationState();

  static dynamic show(BuildContext context, String title, int error, {int multiplePop=1}) async {
    return showDialog(
      context: context,
      builder: (BuildContext context) => ResponseConfirmation(
        title: title,
        error: error,
        multiplePop: multiplePop
      )
    );
  }
}

class _ResponseConfirmationState extends State<ResponseConfirmation> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.title, textAlign: TextAlign.center,),
      titlePadding:
          const EdgeInsets.only(top: 20, bottom: 0, left: 20, right: 20),
      actions: <Widget>[
        TextButton(
          child: const Text("OK"),
          onPressed: () {
            for (int i = 0; i < widget.multiplePop; i++) {
              Navigator.of(context).pop();
            }
          },
        ),
      ],
    );
  }
}
