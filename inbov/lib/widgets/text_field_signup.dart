import '../utils/validators.dart';
import '../widgets/style_textformfield.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class TextFieldSignUp extends StatelessWidget {
  final bool loading;
  final String text;
  final TextEditingController controller;
  final TextCapitalization? capitalization;
  final TextInputType textInputType;
  final Iterable<String> autofillHints;
  final String? mask;
  final Function(String)? validate;
  final TextEditingController? validationController;
  final bool obscure;
  final String? hintText;
  final ValueChanged<String>? onChanged;
  final InputDecoration? decoration;
  
  const TextFieldSignUp({
    Key? key,
    required this.loading,
    required this.text,
    required this.controller,
    required this.textInputType,
    required this.autofillHints,
    this.mask,
    this.validate,
    this.validationController,
    this.hintText,
    this.capitalization,
    required this.obscure,
    this.onChanged,
    this.decoration,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        textCapitalization: capitalization ?? TextCapitalization.none,
        onChanged: onChanged,
        enabled: !loading,
        obscureText: obscure,
        controller: controller,
        autofillHints: loading ? null : autofillHints,
        keyboardType: textInputType,
        inputFormatters: mask != null
            ? <MaskTextInputFormatter>[
                MaskTextInputFormatter(
                  mask: mask,
                ),
              ]
            : null,
        validator: (String? value) {
          if (validate != null) {
            return validate!(value!);
          }
          if (validationController != null) {
            return confirmPasswordValidator(value, validationController!);
          }
          return null;
        },
        decoration: decoration ?? InputDecoration(
          enabledBorder: customInputBorder(),
          focusedBorder: customFocusBorder(),
          border: customInputBorder(),
          hintText: hintText ?? '',
          labelText: text,
        )
    );
  }
}
