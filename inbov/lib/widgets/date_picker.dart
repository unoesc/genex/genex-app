import 'package:inbov/configs.dart';
import 'package:flutter_date_pickers/flutter_date_pickers.dart' as dp;
import 'package:flutter/material.dart';

class DatePicker extends StatefulWidget {
  final DateTime initialDate;
  final bool monthMode;
  final DateTime minDate;
  final DateTime maxDate;
  const DatePicker({
    Key? key,
    required this.initialDate,
    required this.monthMode,
    required this.minDate,
    required this.maxDate,
  }) : super(key: key);

  @override
  _DatePickerState createState() => _DatePickerState();
  static Future<DateTime?> show(BuildContext context, DateTime initialDate, DateTime minDate, DateTime maxDate,
      {bool monthMode = false}) async {
    return showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) => DatePicker(
        initialDate: initialDate,
        monthMode: monthMode,
        minDate: minDate,
        maxDate: maxDate,
      ),
    );
  }
}

class _DatePickerState extends State<DatePicker> {
  DateTime _selectedDate = DateTime(
      DateTime.now().year - 18, DateTime.now().month, DateTime.now().day);

  @override
  void initState() {
    super.initState();
    _selectedDate = widget.initialDate;
  }

  Widget _datePicker(Function picker) => picker(
        selectedDate: _selectedDate,
        firstDate: widget.minDate,
        lastDate: widget.maxDate,
        onChanged: (DateTime date) {
          setState(() {
            _selectedDate = date;
          });
        },
        datePickerStyles: dp.DatePickerRangeStyles(
          selectedSingleDateDecoration: BoxDecoration(
            color: primary,
            borderRadius: radiusBorder,
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: _datePicker(
          widget.monthMode ? dp.MonthPicker.single : dp.DayPicker.single),
      actions: <Widget>[
        TextButton(
            style: TextButton.styleFrom(primary: Colors.grey.shade800),
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text('CANCELAR')),
        TextButton(
          style: TextButton.styleFrom(primary: primary),
          child: const Text('OK'),
          onPressed: () {
            Navigator.of(context).pop(_selectedDate);
          },
        ),
      ],
    );
  }
}
