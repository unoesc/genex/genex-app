import 'package:flutter/material.dart';
import 'package:inbov/models/animal_model.dart';
import 'package:inbov/widgets/confirm_dialog.dart';
import 'package:inbov/widgets/response_confirmation.dart';
import 'package:inbov/widgets/text_field.dart';
import 'package:inbov/widgets/unexpected_error.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import '../configs.dart';
// ignore: library_prefixes
import '../http_requests/http_animal_history.dart' as dioAnimalHistory;
// ignore: library_prefixes
import '../http_requests/http_semi.dart' as dioSemi;
import '../models/area_model.dart';
import '../models/semi_model.dart';
import '../utils/validators.dart';

class AlterHistoryPage extends StatefulWidget{
  final GetAnimal animal;
  final AreaGet area;
  const AlterHistoryPage({
    Key? key,
    required this.animal,
    required this.area,
  }) : super(key: key);

  @override
  State<AlterHistoryPage> createState() => _AlterHistoryPageState();
}

class _AlterHistoryPageState extends State<AlterHistoryPage>{
  final TextEditingController _publicSemiBreedController = TextEditingController();
  final TextEditingController _publicSemiBullController = TextEditingController();
  final GlobalKey<FormState> _publicSemiFormKey = GlobalKey<FormState>();

  int isPublic = 0;

  List<SemisGet> semis = [];
  List<String> availableBreeds = [];
  late String selectedBreed;
  late Semi selectedSemi;
  List<Semi> breedSemis = [];

  List<String> statusOptions = [];
  late String selectedStatus;

  bool semiEmpty = false;
  bool loadingSemis = false;
  bool semiError = false;
  bool loading = false;
  bool error = false;
  bool requestLoading = false;

  @override
  void initState() {
    super.initState();
    getStatusOptions();
    getAvailableSemis();
  }

  Future<void> getStatusOptions() async {
    setState(() {
      loading = true;
      error = false;
    });
    try {
      List<String> options = await dioAnimalHistory.getStatusOptions(widget.animal.id, widget.animal.area_id);
      setState(() {
        statusOptions = options;
        selectedStatus = statusOptions[0];
      });
    }catch(e, stack) {
      print(e);
      print(stack);
      setState(() {
        error = true;
      });
    }finally{
      setState(() {
        loading = false;
      });
    }
  }


  void setBreedSemis() {
    SemisGet superSemi = semis.firstWhere((SemisGet semi) => semi.group_name == selectedBreed);
    setState(() {
      breedSemis = superSemi.semis;
      selectedSemi = breedSemis[0];
    });
  }

  Future<void> getAvailableSemis() async {
    if (!widget.area.gene_activated){
      return;
    }
    setState(() {
      loadingSemis = true;
      semiError = false;
      semiEmpty = false;
    });
    try {
      List<SemisGet> semis = await dioSemi.getSemis(widget.animal.area_id);
      setState(() {
        this.semis = semis;
        if (semis.isEmpty){
          semiEmpty = true;
        }else{
          availableBreeds = [for (SemisGet semi in semis) semi.group_name];
          selectedBreed = availableBreeds[0];
        }
      });
      setBreedSemis();
    }catch(e, stack) {
      print(e);
      print(stack);
      setState(() {
        semiError = true;
      });
    }finally{
      setState(() {
        loadingSemis = false;
      });
    }
  }


  Future<bool> isValid() async {
    if (selectedStatus == "Inseminada") {
      if (isPublic == 0) {
        await ResponseConfirmation.show(context, "Para cadastrar uma inseminação, você deve informar o sêmen usado", 100);
        return false;
      }
      else if (isPublic == 1) {
        if (_publicSemiFormKey.currentState!.validate()) {
          return true;
        }
        else{
          return false;
        }
      }
      else {
        if (semiEmpty) {
          await ResponseConfirmation.show(context, "Para cadastrar uma inseminação, você deve informar o sêmen usado", 100);
          return false;
        }
        else{
          return true;
        }
      }
    }else{
      return true;
    }
  }





  Future<void> submitFunction() async {
    setState(() {
      requestLoading = true;
    });
    try{
      Map<String, dynamic> data = {
        'area_id': widget.animal.area_id,
        'animal_id': widget.animal.id,
        'status': selectedStatus,
        'private_semi_id': isPublic == 2 ? selectedSemi.id : null,
        'public_semi_breed': isPublic == 1 ? _publicSemiBreedController.text : null,
        'public_semi_bull_id': isPublic == 1 ? _publicSemiBullController.text : null,
      };
      await dioAnimalHistory.updateHistory(data);
      //ignore: use_build_context_synchronously
      Navigator.pop(context);
    }catch(e, stack){
      print(e);
      print(stack);
      if (e == 404){
        ResponseConfirmation.show(context, "Animal não encontrado", 404);
      }
      else{
        ResponseConfirmation.show(context, "Ocorreu um erro interno, tente mais tarde", 500);
      }
    }finally{
      setState(() {
        requestLoading = false;
      });
    }
  }


  Widget statusDropDown() {
    return Material(
        borderRadius: BorderRadius.circular(10),
        elevation: 0,
        color: Colors.white,
        child: Container(
          width: MediaQuery.of(context).size.width * 0.8,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: secondary,
              width: 1,
            ),
          ),
          child:  DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                value: selectedStatus,
                onChanged: (String? newValue) async {
                  if (newValue != null) {
                    setState(() {
                      selectedStatus = newValue;
                      isPublic = 0;
                    });
                  }
                },
                items: statusOptions.map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value, style: TextStyle(
                      color: switchAnimalColor(value),
                    ),
                    ),
                  );
                }).toList(),
              )
          ),
        )
    );
  }


  Widget animalCard() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: Column(
        children: [
          const Text(
            "Animal",
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(height: 10),
          Text(
            "Identificação: ${widget.animal.id}",
            style: const TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }


  Widget semiCheckBox(){
    return Column(
      children: <Widget>[
        const SizedBox(height: 20),
        const Text('O sêmen usado é publico ou privado?',
            style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold
            ),
            textAlign: TextAlign.center
        ),
        const SizedBox(height: 10),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            for (int i = 1; i <= 2; i++)
              Row(
                  children: <Widget>[
                    Radio(
                      value: i,
                      groupValue: isPublic,
                      onChanged: (int? value) {
                        setState(() {
                          if (value != null) {
                            isPublic = value;
                          }
                        });
                      },
                    ),
                    Text(
                      i == 1 ? 'Público' : 'Privado',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    const SizedBox(width: 20),
                  ]
              )
          ],
        ),
      ],
    );
  }

  Widget _entryField(String title, TextEditingController controller, Function(String)? validate, TextInputType inputType, {String? mask}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 20),
        TextFieldPage(
          controller: controller,
          loading: loading,
          obscure: false,
          text: title,
          validate: validate,
          focus: false,
          outlined: true,
          typeInput: inputType,
          mask: mask,
        ),
      ],
    );
  }



  Widget publicSemiForm() {
    return SizedBox(
      width: 350,
      child: AutofillGroup(
          child: Form(
            key: _publicSemiFormKey,
            autovalidateMode: AutovalidateMode.onUserInteraction,
            child: Container(
              color: Colors.white,
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                children: <Widget>[
                  _entryField("Raça do sêmen público *", _publicSemiBreedController, requiredFieldValidator, TextInputType.text),
                  _entryField("Identificação do touro *", _publicSemiBullController, requiredFieldValidator, TextInputType.text),
                ],
              ),
            ),
          )
      )
    );
  }


  Widget privateSemiForm() {
    return Container(
        child: (){
          if(loadingSemis){
            return Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 10),
                    Text('Carregando os sêmens disponíveis...'),
                  ]),
            );
          }
          else if (semiEmpty){
            return Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    SizedBox(height: 10),
                    Text('Nenhum sêmen disponível para o animal'),
                    SizedBox(height: 10),
                    Text('Adicione mais na aba "Sêmens"'),
                  ]),
            );
          }
          else{
            return SizedBox(
                width: 350,
                child: Column(
                  children: <Widget>[
                    const SizedBox(height: 20),
                    const Text('Selecione a raça do sêmen',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold
                        ),
                        textAlign: TextAlign.center
                    ),
                    const SizedBox(height: 10),
                    breedDropDown(),
                    const SizedBox(height: 20),
                    const Text('Selecione o sêmen',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold
                        ),
                        textAlign: TextAlign.center
                    ),
                    const SizedBox(height: 10),
                    semiDropDown(),
                  ],
              ),
            );
          }
        }(),
    );
  }




  Widget semiDropDown(){
    return Material(
        borderRadius: BorderRadius.circular(10),
        elevation: 0,
        color: Colors.white,
        child: Container(
          width: MediaQuery.of(context).size.width * 0.8,
          height: 60,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: secondary,
              width: 1,
            ),
          ),
          child:  DropdownButtonHideUnderline(
              child: DropdownButton2<Semi>(
                value: selectedSemi,
                onChanged: (Semi? newValue) async {
                  if (newValue != null) {
                    setState(() {
                      selectedSemi = newValue;
                    });
                  }
                },
                dropdownOverButton: true,
                items: breedSemis.map((Semi value) {
                  return DropdownMenuItem<Semi>(
                    value: value,
                    child: Column(
                        children: [
                          Text(
                            "Touro: ${value.bull_id}",
                            style: const TextStyle(
                              color: Colors.black,
                            ),
                          ),
                          const SizedBox(height: 5),
                          Text(
                            dateFormat(value.acquisition_datetime),
                            style: const TextStyle(
                              color: Colors.black,
                            ),
                          ),
                        ],
                      )
                  );
                }).toList(),
              )
          ),
        )
    );
  }


  Widget breedDropDown() {
    return Material(
        borderRadius: BorderRadius.circular(10),
        elevation: 0,
        color: Colors.white,
        child: Container(
          width: MediaQuery.of(context).size.width * 0.8,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: secondary,
              width: 1,
            ),
          ),
          child:  DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                value: selectedBreed,
                onChanged: (String? newValue) async {
                  if (newValue != null) {
                    setState(() {
                      selectedBreed = newValue;
                    });
                    setBreedSemis();
                  }
                },
                items: availableBreeds.map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value, style: TextStyle(
                      color: switchAnimalColor(value),
                    ),
                    ),
                  );
                }).toList(),
              )
          ),
        )
    );
  }


  Widget submitButton() {
    return InkWell(
      onTap: () async {
        if (await isValid()){
          if (await ConfirmDialog.show(context, "Tem certeza que deseja salvar?", text: "As gravações serão salvas e não podem ser desfeitas")){
            await submitFunction();
          }
        }
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: const LinearGradient(
            colors: [
              Color(0xFF4caf50),
              Color(0xFF8bc34a),
            ],
          ),
        ),
        child: const Text(
          'Salvar',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Alterar Status do Animal'),
      ),
      body: RefreshIndicator(
        onRefresh: getStatusOptions,
        child: (){
          if (loading){
            return Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 10),
                    Text('Carregando as informações...'),
                  ]),
            );
          }else if (error){
            return UnexpectedError(onRefresh: getStatusOptions,);
          }
          else{
            return SingleChildScrollView(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    animalCard(),
                    const Divider(color: secondary, height: 2,),
                    const SizedBox(height: 10),
                    statusDropDown(),
                    if (selectedStatus == "Inseminada" && widget.area.gene_activated == true)
                      semiCheckBox()
                    else if (selectedStatus == "Inseminada" && widget.area.gene_activated == false)
                      publicSemiForm(),
                    if (isPublic == 1)
                      publicSemiForm()
                    else if (isPublic == 2)
                      privateSemiForm(),
                    const SizedBox(height: 10),
                    submitButton(),
                  ],
                ),
              )
            );
          }
        }(),
      )
    );
  }
}