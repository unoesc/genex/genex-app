import '../configs.dart';
import 'package:flutter/material.dart';

class UnexpectedError extends StatelessWidget {
  final Function() onRefresh;
  const UnexpectedError({Key? key, required this.onRefresh}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
          const Text(
            "Ocorreu um erro inesperado",
            style: TextStyle(fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 5),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              primary: primary,
              padding: const EdgeInsets.all(10),
            ),
            child: const Text("Tentar Novamente"),
            onPressed: onRefresh,
          ),
          const SizedBox(height: 20),
        ]));
  }
}
