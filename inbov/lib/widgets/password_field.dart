import '../configs.dart';
import '../widgets/style_textformfield.dart';
import '../widgets/text_field.dart';
import '../widgets/text_field_signup.dart';
import 'package:flutter/material.dart';

class PasswordField extends StatelessWidget {
  final bool signUp;
  final bool loading;
  final String text;
  final TextEditingController controller;
  final Function(String)? validate;
  final TextEditingController? validationController;
  final bool? newPassword;

  const PasswordField({
    Key? key,
    required this.signUp,
    required this.loading,
    required this.text,
    required this.controller,
    this.validate,
    this.validationController,
    this.newPassword,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool obscureIcon = true;
    return ScaffoldMessenger(
        child: StatefulBuilder(builder: (BuildContext context, setState) {
      return signUp != true
          ? TextFieldPage(
              loading: loading,
              text: text,
              typeInput: TextInputType.visiblePassword,
              typeHints: const <String>[AutofillHints.password],
              controller: controller,
              newPassword: newPassword,
              validate: validate,
              controllerValide: validationController,
              outlined: false,
              obscure: obscureIcon,
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(10),
                border: InputBorder.none,
                labelText: text,
                suffixIcon: IconButton(
                  icon: Icon(
                    obscureIcon ? Icons.visibility : Icons.visibility_off,
                    color: primary,
                  ),
                  onPressed: () {
                    setState(() {
                      obscureIcon = !obscureIcon;
                    });
                  },
                ),
              ),
            focus: false,
            )
          : TextFieldSignUp(
              loading: loading,
              controller: controller,
              obscure: obscureIcon,
              text: text,
              textInputType: TextInputType.visiblePassword,
              autofillHints: const <String>[AutofillHints.password],
              validationController: validationController,
              decoration: InputDecoration(
                enabledBorder: customInputBorder(),
                focusedBorder: customFocusBorder(),
                border: customInputBorder(),
                labelText: text,
                suffixIcon: IconButton(
                  icon: Icon(
                    obscureIcon ? Icons.visibility : Icons.visibility_off,
                    color: primary,
                  ),
                  onPressed: () {
                    setState(() {
                      obscureIcon = !obscureIcon;
                    });
                  },
                ),
              ),
            );
    }));
  }
}
