import 'package:inbov/configs.dart';
import 'package:inbov/utils/validators.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class TextFieldPage extends StatelessWidget {
  final bool loading;
  final String text;
  final TextEditingController controller;
  final TextCapitalization? capitalization;
  final TextInputType? typeInput;
  final Iterable<String>? typeHints;
  final String? mask;
  final String? initialText;
  final Map<String, RegExp>? filter;
  final Function(String)? validate;
  final TextEditingController? controllerValide;
  final bool obscure;
  final bool focus;
  final bool? newPassword;
  final ValueChanged<String>? onChanged;
  final int? lines;
  final String? hintText;
  final IconButton? suffixIcon;
  final InputDecoration? decoration;
  final Function()? onTap;
  final bool? readOnly;
  final bool outlined;
  const TextFieldPage({
    Key? key,
    required this.loading,
    required this.text,
    required this.controller,
    this.typeInput,
    this.onChanged,
    this.typeHints,
    this.mask,
    this.validate,
    this.controllerValide,
    this.newPassword,
    this.lines,
    this.filter,
    this.initialText,
    this.capitalization,
    required this.focus,
    required this.obscure,
    this.decoration,
    this.hintText,
    this.suffixIcon,
    this.onTap,
    this.readOnly,
    required this.outlined,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      decoration: BoxDecoration(
        color: Colors.grey[300],
        borderRadius: radiusBorder,
      ),
      child: TextFormField(
        onChanged: onChanged,
        enabled: !loading,
        obscureText: obscure,
        maxLines: lines ?? 1,
        keyboardType: typeInput ?? TextInputType.name,
        autocorrect: false,
        textCapitalization: capitalization ?? TextCapitalization.none,
        controller: controller,
        autofillHints: loading ? null : typeHints,
        autofocus: focus,
        inputFormatters: mask != null
            ? <MaskTextInputFormatter>[
                MaskTextInputFormatter(
                    mask: mask, filter: filter, initialText: initialText),
              ]
            : null,
        validator: (String? value) {
          if (validate != null) {
            return validate!(value!);
          }
          if (newPassword == true) {
            return confirmAlterPasswordValidator(value, controllerValide!);
          }
          if (controllerValide != null) {
            return confirmPasswordValidator(value, controllerValide!);
          }
          return null;
        },
        cursorColor: Colors.black,
        decoration: decoration ?? (
            outlined == true
            ? InputDecoration(
                border: const UnderlineInputBorder(),
                labelText: text,
                hintText: hintText,
                contentPadding: const EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                hintStyle: TextStyle(
                  color: Colors.grey[700],
                ),
                suffixIcon: suffixIcon,
                labelStyle: const TextStyle(
                  color: secondary,
                  fontSize: 18,
                ),
                focusedBorder: const UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: secondary,
                      width: 1,
                    )),
                enabledBorder: const UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: secondary,
                      width: 1,
                    )),
              )
        : InputDecoration(
            contentPadding: const EdgeInsets.all(10),
            border: InputBorder.none,
            labelText: text,
            hintText: hintText,
          )),
        onTap: onTap,
        readOnly: readOnly ?? false,
      ),
    );
  }
}
