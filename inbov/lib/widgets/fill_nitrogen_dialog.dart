import 'package:date_time_picker/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:inbov/widgets/response_confirmation.dart';
import 'package:inbov/widgets/text_field.dart';
//ignore: library_prefixes
import '../../http_requests/http_gene_controller.dart' as dioGene;
import '../configs.dart';
import '../models/area_model.dart';
import '../utils/validators.dart';

class FillNitrogenDialog extends StatefulWidget {
  final AreaGet area;
  const FillNitrogenDialog({
    Key? key,
    required this.area,
  }) : super(key: key);
  @override
  State<FillNitrogenDialog> createState() => _FillNitrogenDialogState();

  static Future<void> show(BuildContext context, AreaGet area) async {
    await showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return FillNitrogenDialog(area: area);
      },
    );
  }
}

class _FillNitrogenDialogState extends State<FillNitrogenDialog> {
  final GlobalKey<FormState> _geneFormKey = GlobalKey<FormState>();
  TextEditingController nitrogenStatus = TextEditingController();
  TextEditingController nitrogenLossTax = TextEditingController();
  DateTime nitrogenNextRefill = DateTime.now().add(const Duration(days: 1));

  bool loading = false;


  Widget _entryField(String title, TextEditingController controller, Function(String)? validate, TextInputType inputType, {String? mask}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 20),
        TextFieldPage(
          controller: controller,
          loading: loading,
          obscure: false,
          text: title,
          validate: validate,
          focus: false,
          outlined: true,
          typeInput: inputType,
          mask: mask,
        ),
      ],
    );
  }


  Widget _calendarPicker(String title, DateTime? pickedDate, DateTime firstDate, DateTime lastDate, DateTime date, Function(String) onDateChanged) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 20),
        Container(
            margin: const EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
              color: Colors.grey[300],
              borderRadius: radiusBorder,
            ),
            child:
            DateTimePicker(
              type: DateTimePickerType.date,
              dateMask: 'dd/MM/yyyy',
              initialDate: pickedDate,
              initialValue: date.toString(),
              initialDatePickerMode: DatePickerMode.day,
              firstDate: firstDate,
              lastDate: lastDate,
              style: const TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
              ),
              calendarTitle: 'SELECIONE A DATA',
              decoration: InputDecoration(
                border: const UnderlineInputBorder(),
                labelText: title,
                contentPadding: const EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 10,
                ),
                hintStyle: TextStyle(
                  color: Colors.grey[700],
                ),
                suffixIcon: Icon(
                  Icons.calendar_today,
                  color: Colors.grey[700],
                ),
                labelStyle: const TextStyle(
                  color: secondary,
                  fontSize: 18,
                ),
                focusedBorder: const UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: secondary,
                      width: 1,
                    )),
                enabledBorder: const UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: secondary,
                      width: 1,
                    )),
              ),
              onChanged: onDateChanged,
            )
        ),
      ],
    );
  }


  Widget _geneForm() {
    return AutofillGroup(
        child: Form(
          key: _geneFormKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Container(
            color: Colors.white,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: <Widget>[
                _entryField("Quantidade (Litros) *", nitrogenStatus, requiredFieldValidator, TextInputType.number, mask: '##.##'),
                _entryField("Taxa de perda (Litros/dia) *", nitrogenLossTax, requiredFieldValidator, TextInputType.number, mask: '##.##'),
                _calendarPicker(
                    "Próximo abastecimento *",
                    nitrogenNextRefill,
                    DateTime.now().add(const Duration(days: 1)),
                    DateTime.now().add(const Duration(days: 365)),
                    nitrogenNextRefill,
                        (String value) {
                      setState(() {
                        nitrogenNextRefill = DateTime.parse(value);
                      });
                    }
                ),
              ],
            ),
          ),
        )
    );
  }

  Future<void> submitFunction() async {
    setState(() {
      loading = true;
    });
    try{
      Map<String, dynamic> data = {
        "area_id": widget.area.id,
        "nitrogen_LT_status": double.parse(nitrogenStatus.text),
        "nitrogen_day_loss_tax": double.parse(nitrogenLossTax.text),
        "nitrogen_next_refill": nitrogenNextRefill.toString().split(' ')[0],
      };
      await dioGene.updateGeneController(data);
      //ignore: use_build_context_synchronously
      await ResponseConfirmation.show(context, "Nitrogênio atualizado com sucesso", 200);
      //ignore: use_build_context_synchronously
      Navigator.of(context).pop();
    }catch(e, stacktrace){
      print(e);
      print(stacktrace);
      if (e == 404){
        ResponseConfirmation.show(context, "Area não encontrada", 404);
      }
      else{
        ResponseConfirmation.show(context, "Ocorreu um erro interno, tente mais tarde", 500);
      }
    }finally{
      setState(() {
        loading = false;
      });
    }
  }


  Widget submitButton() {
    return InkWell(
      onTap: () async {
        if (_geneFormKey.currentState!.validate()) {
          await submitFunction();
        }
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: const LinearGradient(
            colors: [
              Color(0xFF4caf50),
              Color(0xFF8bc34a),
            ],
          ),
        ),
        child: const Text(
          'Salvar',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }




  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: AlertDialog(
          contentPadding: const EdgeInsets.all(20),
          content: SizedBox(
            width: 400,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    children: [
                      const Text(
                          "Nítrogênio",
                          style:
                          TextStyle(
                              fontSize: 20,
                              color: secondary,
                              fontWeight: FontWeight.w800
                          )
                      ),
                      const Spacer(),
                      IconButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: const Icon(Icons.close)
                      ),
                  ],
                ),
                const SizedBox(height: 20),
                _geneForm(),
                const SizedBox(height: 10),
                SizedBox(
                    width: double.infinity,
                    child: loading
                        ? const Center(
                            child: CircularProgressIndicator(),
                          )
                        : submitButton()
                )
              ],
          ),
        ),
      ),
    );
  }

}