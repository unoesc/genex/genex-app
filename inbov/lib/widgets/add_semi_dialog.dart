import 'package:flutter/material.dart';
import 'package:inbov/utils/validators.dart';
import 'package:inbov/widgets/response_confirmation.dart';
import 'package:inbov/widgets/text_field.dart';
//ignore: library_prefixes
import '../../http_requests/http_semi.dart' as dioSemi;
import '../configs.dart';
import '../models/area_model.dart';

class AddSemiDialog extends StatefulWidget {
  final AreaGet area;
  const AddSemiDialog({
    Key? key,
    required this.area,
  }) : super(key: key);
  @override
  State<AddSemiDialog> createState() => _AddSemiDialogState();

  static Future<void> show(BuildContext context, AreaGet area) async {
    await showDialog<void>(
      context: context,
      builder: (BuildContext context) {
        return AddSemiDialog(area: area);
      },
    );
  }
}

class _AddSemiDialogState extends State<AddSemiDialog>{
  final TextEditingController _semiBreedController = TextEditingController();
  final TextEditingController _semiBullController = TextEditingController();
  final TextEditingController _semiQuantityController = TextEditingController(
    text: "1",
  );

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool loading = false;


  Widget _entryField(String title, TextEditingController controller, Function(String)? validate, TextInputType inputType, {String? mask}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 10),
        TextFieldPage(
          controller: controller,
          loading: loading,
          obscure: false,
          text: title,
          validate: validate,
          focus: false,
          outlined: true,
          typeInput: inputType,
          mask: mask,
        ),
      ],
    );
  }


  Future<void> submitFunction() async {
    setState(() {
      loading = true;
    });
    try{
      Map<String, dynamic> data = {
        "area_id": widget.area.id,
        "breed": _semiBreedController.text,
        "bull_id": _semiBullController.text,
        "quantity": int.parse(_semiQuantityController.text),
      };
      await dioSemi.createSemi(data);
      //ignore: use_build_context_synchronously
      await ResponseConfirmation.show(context, "Sêmens cadastrados com sucesso", 200);
      //ignore: use_build_context_synchronously
      Navigator.of(context).pop();
    }catch(e, stacktrace){
      print(e);
      print(stacktrace);
      if (e == 404){
        ResponseConfirmation.show(context, "Area não encontrada", 404);
      }
      else{
        ResponseConfirmation.show(context, "Ocorreu um erro interno, tente mais tarde", 500);
      }
    }finally{
      setState(() {
        loading = false;
      });
    }
  }


  Widget submitButton() {
    return InkWell(
      onTap: () async {
        if (_formKey.currentState!.validate()) {
          await submitFunction();
        }
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: const LinearGradient(
            colors: [
              Color(0xFF4caf50),
              Color(0xFF8bc34a),
            ],
          ),
        ),
        child: const Text(
          'Salvar',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }



  Widget semiForm() {
    return AutofillGroup(
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              _entryField(
                "Raça",
                _semiBreedController,
                requiredFieldValidator,
                TextInputType.text,
              ),
              _entryField(
                "Indentificação do touro",
                _semiBullController,
                requiredFieldValidator,
                TextInputType.text,
              ),
              _entryField(
                "Quantidade",
                _semiQuantityController,
                requiredFieldValidator,
                TextInputType.number,
                mask: '###'
              ),
            ],
          ),
        ),
    );
  }



  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: AlertDialog(
        contentPadding: const EdgeInsets.all(20),
        content: SizedBox(
          width: 400,
          child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  children: [
                    const Text(
                        "Adicionar sêmens",
                        style:
                        TextStyle(
                            fontSize: 20,
                            color: secondary,
                            fontWeight: FontWeight.w800
                        )
                    ),
                    const Spacer(),
                    IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: const Icon(Icons.close)
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                semiForm(),
                const SizedBox(height: 20),
                SizedBox(
                    width: double.infinity,
                    child: loading
                        ? const Center(
                      child: CircularProgressIndicator(),
                    )
                        : submitButton()
                )
              ],
          ),
        ),
      ),
    );
  }

}