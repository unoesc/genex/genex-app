import 'package:flutter/material.dart';
import 'dart:async';
import 'package:inbov/configs.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'models/user_model.dart';

void main() async => runApp(const App());

class App extends StatefulWidget {
  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}


class _AppState extends State<App> {
  final int splashScreenTime = 2000;
  bool splashScreenVisible = false;
  late DateTime start;

  @override
  void initState() {
    start = DateTime.now();
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      initialNavigation();
    });
  }

  Future<String> decideFirstPage() async {
    bool isLogged = await api.isLogged();
    LoginUser? user = await api.getUserData();
    if (isLogged || user != null) {
        return '/area-area';
    }
    return '/login-area';
  }

  void initialNavigation() async {
    await Future<void>.delayed(const Duration(milliseconds: 100));
    setState(() {
      splashScreenVisible = true;
    });
    String firstPage = await decideFirstPage();
    DateTime end = DateTime.now();
    int elapsed = end.difference(start).inMilliseconds;
    await Future<void>.delayed(
        Duration(milliseconds: splashScreenTime - elapsed)
    );
    await Navigator.pushReplacementNamed(
        App.navigatorKey.currentContext!,
        firstPage
    );
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitDown,
        DeviceOrientation.portraitUp,
      ],
    );
    return MaterialApp(
      title: 'Inbov',
      routes: routesApp,
      navigatorKey: App.navigatorKey,
      localizationsDelegates: GlobalMaterialLocalizations.delegates,
      supportedLocales: const <Locale>[Locale('pt', 'BR')],
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      theme: themeLight,
    );
  }
}
