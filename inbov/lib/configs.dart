import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:inbov/pages/add_area_page.dart';
import 'package:inbov/pages/area_page.dart';
import 'package:inbov/pages/login_page.dart';
import 'package:inbov/pages/signup_page.dart';
import 'package:intl/intl.dart';
import 'http_requests/api.dart';

API api = API();


final Map<String, WidgetBuilder> routesApp = <String, WidgetBuilder>{
  '/': (BuildContext context) => _splashScreen(),
  '/login-area': (BuildContext context) => const LoginPage(),
  '/area-area': (BuildContext context) => const AreasPage(),
  '/signup': (BuildContext context) => const SignupPage(),
  '/add-area': (BuildContext context) => const AddAreaPage(),
};

// API URL
// ignore: constant_identifier_names
const String BASE_URL = String.fromEnvironment(
  'BASE_URL',
  defaultValue: 'http://18.234.81.62:8000',
);


NumberFormat moneyFormat =
    NumberFormat.currency(locale: "pt-BR", symbol: "R\$");


String dateFormat(DateTime date, {bool withTime = false}) {
  if (withTime) {
    return DateFormat('HH:mm - dd/MM/yyyy').format(date);
  } else {
    return DateFormat('dd/MM/yyyy').format(date);
  }
}


String dateToBackEnd(DateTime date) {
  return date.toString().split(' ')[0];
}

String monthFormat(DateTime date, {bool withYear = false}) {
  switch(date.month){
    case 1:
      return withYear ? 'Janeiro de ${date.year}' : 'Janeiro';
    case 2:
      return withYear ? 'Fevereiro de ${date.year}' : 'Fevereiro';
    case 3:
      return withYear ? 'Março de ${date.year}' : 'Março';
    case 4:
      return withYear ? 'Abril de ${date.year}' : 'Abril';
    case 5:
      return withYear ? 'Maio de ${date.year}' : 'Maio';
    case 6:
      return withYear ? 'Junho de ${date.year}' : 'Junho';
    case 7:
      return withYear ? 'Julho de ${date.year}' : 'Julho';
    case 8:
      return withYear ? 'Agosto de ${date.year}' : 'Agosto';
    case 9:
      return withYear ? 'Setembro de ${date.year}' : 'Setembro';
    case 10:
      return withYear ? 'Outubro de ${date.year}' : 'Outubro';
    case 11:
      return withYear ? 'Novembro de ${date.year}' : 'Novembro';
    case 12:
      return withYear ? 'Dezembro de ${date.year}' : 'Dezembro';
    default :
      return 'Mês inválido';
  }
}

// Color palette
const Color primary = Color(0xff43A047);
//const Color secondary = Color(0xff51A2D5);
const Color secondary = Color(0xff2E7D32);
//const Color backgroundColor = Color(0xffb0eab2);
const Color backgroundColor = Colors.white;
const Color textColor = Colors.white;

// Border
BorderRadius radiusBorder = BorderRadius.circular(5);

StatefulWidget _splashScreen() => FutureBuilder<bool>(
      initialData: false,
      future: Future<bool>.sync(() async {
        await Future<void>.delayed(const Duration(milliseconds: 100));
        return true;
      }),
      builder: (BuildContext context, AsyncSnapshot<bool> snapshot) =>
          Container(
        color: Colors.white,
        child: Center(
          child: AnimatedOpacity(
            duration: const Duration(milliseconds: 200),
            opacity: snapshot.data == true ? 1 : 0,
            child: Image.asset('assets/icon.png', width: 200),
          ),
        ),
      ),
    );


bool isDesktop() {
  TargetPlatform platform = defaultTargetPlatform;
  if (platform == TargetPlatform.windows ||
      platform == TargetPlatform.macOS ||
      platform == TargetPlatform.linux) {
    return true;
  } else {
    return false;
  }
}

bool notIsMobile(BuildContext context) {
  double width = MediaQuery.of(context).size.width;
  if (!kIsWeb && !isDesktop() || width <= 950) {
    return true;
  } else {
    return false;
  }
}


ThemeData themeLight = ThemeData(
  colorScheme: const ColorScheme.light(
    primary: primary,
    secondary: secondary,
  ),
  primaryColor: primary,
  backgroundColor: backgroundColor,
  scaffoldBackgroundColor: backgroundColor,
  inputDecorationTheme: const InputDecorationTheme(
    border: OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.grey,
        width: 1,
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.grey,
        width: 1,
      ),
    ),
    focusedBorder: OutlineInputBorder(
      borderSide: BorderSide(
        color: Colors.grey,
        width: 1,
      ),
    ),
  ),
);



Color switchAnimalColor(String status) {
  switch (status) {
    case 'Desconhecido':
      return Colors.grey;
    case 'Inseminada':
      return Colors.green.shade200;
    case 'Em gestação':
      return Colors.green.shade600;
    case 'Seca':
      return Colors.orange.shade600;
    case 'Lactante':
      return Colors.yellow.shade700;
    case 'Repetindo':
      return Colors.red.shade300;
    case 'No cio':
      return Colors.blue.shade400;
    case 'Cria perdida':
      return Colors.red.shade700;
    case 'Na engorda':
      return Colors.purple;
    case 'Todos':
      return Colors.black;
    default:
      return Colors.grey;
  }
}