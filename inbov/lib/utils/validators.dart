import 'package:flutter/material.dart';

String? emailValidator(dynamic value) {
  if (value.isEmpty) {
    return ("Campo Obrigatório");
  }
  const String pattern = r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$';
  final RegExp regExp = RegExp(pattern);

  if (!regExp.hasMatch(value)) {
    return ("Informe um e-mail válido");
  }
  return null;
}

String? linkValidator(dynamic value) {
  if (value.isEmpty) {
    return ("Campo Obrigatório");
  }

  ///(?:(?:https?|ftp):\/\/)?[\w/\-?=%.]+\.[\w/\-?=%.]+ regex velho
  const String pattern =
      r'(?:(?:https?|ftp):(\/\/){1})+[\w\-?=%.]+[\w/\-?=%.]+\.[\w/\-?=%.]+'; //Não deixa ter 3 baras seguidas
  final RegExp regExp = RegExp(pattern);

  if (!regExp.hasMatch(value)) {
    return ("Informe um Link válido");
  }
  return null;
}

String? phoneValidator(dynamic value) {
 if ((value.isNotEmpty) && (value.length < 14 || value.length > 15)) {
    return 'Utilize um número de celular válido';
  } else {
    return null;
  }
}

String? passwordValidator(dynamic value) {
  bool passValid =
      RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$').hasMatch(value!);
  if (value.isEmpty) {
    return "Campo obrigatório";
  } else if (!passValid) {
    return "Requisitos ausentes";
  }
  return null;
}

String? codeValidator(dynamic value) {
  if (value == null || value.isEmpty) {
    return ("Campo Obrigatório");
  }
  else if ( value.length != 8){
    return ("O código deve ter 8 dígitos");
  }
  return null;
}

String? confirmAlterPasswordValidator(
    dynamic value, TextEditingController passwordController) {
  if (value!.isEmpty) {
    return 'Campo obrigatório';
  }
  if (value == passwordController.text) {
    return 'A nova senha não pode ser igual a anterior';
  }
  return null;
}

String? confirmPasswordValidator(
    dynamic value, TextEditingController passwordController) {
  if (value!.isEmpty) {
    return 'Campo obrigatório';
  }
  if (value != passwordController.text) {
    return 'Senha diferente da anterior';
  }
  return null;
}

String? requiredFieldValidator(dynamic value) {
  if (value == null || value.isEmpty) {
    return ("Campo Obrigatório");
  }
  return null;
}

String? arrobaValidator(dynamic value) {
  if (value.isEmpty) {
    return ("Campo Obrigatório");
  }
  const String pattern = r'^@+[\w]+';
  final RegExp regExp = RegExp(pattern);

  if (!regExp.hasMatch(value)) {
    return ("Informe um @ válido");
  }
  return null;
}
