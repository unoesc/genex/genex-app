String repeat(String string, int times) {
  return List.generate(times, (i) => string).join();
}


String? emailConcealer(String? email) {
  if (email == null) {
    return null;
  }
  return email.replaceRange(3, email.indexOf('@'), repeat('*', email.indexOf('@') - 3));
}


String? cpfConcealer(String? cpf) {
  if (cpf == null) {
    return null;
  }
  return cpf.replaceRange(0, 7, repeat('*', 7));
}