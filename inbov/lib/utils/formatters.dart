String? cpfFormatter(String? cpf) {
  if (cpf == null) {
    return null;
  }
  if (cpf.length > 11) {
    return cpf;
  }
  String data = cpf.replaceRange(3, 3, '.');
  data = data.replaceRange(7, 7, '.');
  data = data.replaceRange(11, 11, '-');
  return data;
}

String getFirstName(fullName) {
  List<String> name = fullName.split(' ');
  return name[0];
}



String removeChars(String value) {
  return value.replaceAll(RegExp(r'[^0-9]'), '');
}
