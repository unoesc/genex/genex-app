class GeneControllerGet{
  bool active;
  // ignore: non_constant_identifier_names
  String sensor_token;
  // ignore: non_constant_identifier_names
  double? nitrogen_LT_status;
  // ignore: non_constant_identifier_names
  double? nitrogen_day_loss_tax;
  // ignore: non_constant_identifier_names
  DateTime? nitrogen_next_refill;
  // ignore: non_constant_identifier_names
  DateTime? nitrogen_last_refill;
  // ignore: non_constant_identifier_names
  double? nitrogen_current_status;
  // ignore: non_constant_identifier_names
  int? available_semis;

  GeneControllerGet({
    required this.active,
    // ignore: non_constant_identifier_names
    required this.sensor_token,
    // ignore: non_constant_identifier_names
    required this.nitrogen_LT_status,
    // ignore: non_constant_identifier_names
    required this.nitrogen_day_loss_tax,
    // ignore: non_constant_identifier_names
    required this.nitrogen_next_refill,
    // ignore: non_constant_identifier_names
    required this.nitrogen_last_refill,
    // ignore: non_constant_identifier_names
    required this.nitrogen_current_status,
    // ignore: non_constant_identifier_names
    required this.available_semis,
  });


  factory GeneControllerGet.fromJson(Map<String, dynamic> json){
    return GeneControllerGet(
      active: json['active'] as bool,
      // ignore: non_constant_identifier_names
      sensor_token: json['sensor_token'] as String,
      // ignore: non_constant_identifier_names
      nitrogen_LT_status: json['nitrogen_LT_status'] as double?,
      // ignore: non_constant_identifier_names
      nitrogen_day_loss_tax: json['nitrogen_day_loss_tax'] as double?,
      // ignore: non_constant_identifier_names
      nitrogen_next_refill: json['nitrogen_next_refill'] == null ? null : DateTime.parse(json['nitrogen_next_refill'] as String),
      // ignore: non_constant_identifier_names
      nitrogen_last_refill: json['nitrogen_last_refill'] == null ? null : DateTime.parse(json['nitrogen_last_refill'] as String),
      // ignore: non_constant_identifier_names
      nitrogen_current_status: json['nitrogen_current_status'] as double?,
      // ignore: non_constant_identifier_names
      available_semis: json['available_semis'] as int?,
    );
  }
}