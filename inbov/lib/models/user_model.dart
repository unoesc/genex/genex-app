class LoginUser{
  final int id;
  final String name;
  final String email;

  LoginUser({
    required this.id,
    required this.name,
    required this.email,
  });

  factory LoginUser.fromJson(Map<String, dynamic> json) {
    return LoginUser(
      id: json['id'] as int,
      name: json['name'] as String,
      email: json['email'] as String,
    );
  }
}

class GetUser{
  String email;
  String name;
  String? phone;

  GetUser({
    required this.email,
    required this.name,
    required this.phone,
  });

  factory GetUser.fromJson(Map<String, dynamic> json) {
    return GetUser(
      email: json['email'] as String,
      name: json['name'] as String,
      phone: json['phone'] as String?,
    );
  }
}