class GetAnimal {
  // ignore: non_constant_identifier_names
  int area_id;
  String id;
  String? name;
  // ignore: non_constant_identifier_names
  String current_status;

  GetAnimal({
    // ignore: non_constant_identifier_names
    required this.area_id,
    required this.id,
    required this.name,
    // ignore: non_constant_identifier_names
    required this.current_status,
  });


  factory GetAnimal.fromJson(Map<String, dynamic> json){
    String? description;
    if (json['description'] != null) {
      description = json['description'] as String;
    }
    String? name;
    if (json['name'] != null) {
      name = json['name'] as String;
    }
    return GetAnimal(
      area_id: json['area_id'] as int,
      id: json['id'] as String,
      name: name,
      current_status: json['current_status'] as String,
    );
  }
}


class GetAnimals{
  List<GetAnimal> animals;
  List<String> filters;
  GetAnimals({
    required this.animals,
    required this.filters,
  });
  factory GetAnimals.fromJson(Map<String, dynamic> json) {
    final List<GetAnimal> animals = (json['animals'] as List).map((i) => GetAnimal.fromJson(i)).toList();
    final List<String> filters = (json['filters'] as List).map((i) => i as String).toList();
    return GetAnimals(
      animals: animals,
      filters: filters,
    );
  }
}