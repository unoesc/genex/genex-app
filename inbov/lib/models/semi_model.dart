class Semi{
  int id;
  String breed;
  // ignore: non_constant_identifier_names
  String bull_id;
  // ignore: non_constant_identifier_names
  DateTime acquisition_datetime;
  // ignore: non_constant_identifier_names
  DateTime? used_datetime;

  Semi({
    required this.id,
    required this.breed,
    // ignore: non_constant_identifier_names
    required this.bull_id,
    // ignore: non_constant_identifier_names
    required this.acquisition_datetime,
    // ignore: non_constant_identifier_names
    required this.used_datetime,
  });

  factory Semi.fromJson(Map<String, dynamic> json){
    // ignore: non_constant_identifier_names
    DateTime? used_datetime;
    if (json['used_datetime'] != null) {
      used_datetime = DateTime.parse(json['used_datetime'] as String);
    }

    return Semi(
      id: json['id'] as int,
      breed: json['breed'] as String,
      // ignore: non_constant_identifier_names
      bull_id: json['bull_id'] as String,
      // ignore: non_constant_identifier_names
      acquisition_datetime: DateTime.parse(json['acquisition_datetime'] as String),
      // ignore: non_constant_identifier_names
      used_datetime: used_datetime,
    );
  }
}


class SemisGet{
  // ignore: non_constant_identifier_names
  String group_name;
  List<Semi> semis;

  SemisGet({
    // ignore: non_constant_identifier_names
    required this.group_name,
    required this.semis,
  });

  factory SemisGet.fromJson(Map<String, dynamic> json){
    return SemisGet(
      group_name: json['group_name'] as String,
      semis: (json['semis'] as List<dynamic>).map((e) => Semi.fromJson(e as Map<String, dynamic>)).toList(),
    );
  }
}