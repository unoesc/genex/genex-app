import 'package:inbov/models/animal_model.dart';
import 'package:inbov/models/semi_model.dart';

class GetHistory {
  // ignore: non_constant_identifier_names
  String animal_id;
  // ignore: non_constant_identifier_names
  DateTime created_datetime;
  String status;
  // ignore: non_constant_identifier_names
  Semi? private_semi;
  // ignore: non_constant_identifier_names
  String? public_semi_breed;
  // ignore: non_constant_identifier_names
  String? public_semi_bull_id;

  GetHistory({
    // ignore: non_constant_identifier_names
    required this.animal_id,
    // ignore: non_constant_identifier_names
    required this.created_datetime,
    required this.status,
    // ignore: non_constant_identifier_names
    this.private_semi,
    // ignore: non_constant_identifier_names
    this.public_semi_breed,
    // ignore: non_constant_identifier_names
    this.public_semi_bull_id,
  });

  factory GetHistory.fromJson(Map<String, dynamic> json){
    return GetHistory(
      animal_id: json['animal_id'] as String,
      created_datetime: DateTime.parse(json['created_datetime'] as String),
      status: json['status'] as String,
      private_semi: json['private_semi'] != null ? Semi.fromJson(json['private_semi'] as Map<String, dynamic>) : null,
      public_semi_breed: json['public_semi_breed'] as String?,
      public_semi_bull_id: json['public_semi_bull_id'] as String?,
    );
  }
}


class GetHistories{
  List<GetHistory> histories = [];
  List<String> filters = [];
  // ignore: non_constant_identifier_names
  DateTime initial_date;

  GetHistories({
    required this.histories,
    required this.filters,
    // ignore: non_constant_identifier_names
    required this.initial_date,
  });
  factory GetHistories.fromJson(Map<String, dynamic> json){
    return GetHistories(
      histories: (json['histories'] as List<dynamic>).map((e) => GetHistory.fromJson(e as Map<String, dynamic>)).toList(),
      filters: (json['filters'] as List<dynamic>).map((e) => e as String).toList(),
      initial_date: DateTime.parse(json['initial_date'] as String),
    );
  }
}


class HistoryEvents{
  String title;
  String type;
  // ignore: non_constant_identifier_names
  DateTime? occurrence_date;
  String? text;
  GetAnimal? animal;

  HistoryEvents({
    required this.title,
    required this.type,
    // ignore: non_constant_identifier_names
    this.occurrence_date,
    this.text,
    this.animal,
  });

  factory HistoryEvents.fromJson(Map<String, dynamic> json){
    return HistoryEvents(
      title: json['title'] as String,
      type: json['type'] as String,
      occurrence_date: json['occurrence_date'] != null ? DateTime.parse(json['occurrence_date'] as String) : null,
      text: json['text'] as String?,
      animal: json['animal'] != null ? GetAnimal.fromJson(json['animal'] as Map<String, dynamic>) : null,
    );
  }
}