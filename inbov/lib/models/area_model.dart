import 'gene_controller_model.dart';

class AreaGet{
  String name;
  int id;
  String? description;
  //ignore: non_constant_identifier_names
  int animals_count;
  //ignore: non_constant_identifier_names
  bool gene_activated;
  //ignore: non_constant_identifier_names
  GeneControllerGet? private_gene;

  AreaGet({
    required this.name,
    required this.id,
    required this.description,
    //ignore: non_constant_identifier_names
    required this.animals_count,
    //ignore: non_constant_identifier_names
    required this.gene_activated,
    //ignore: non_constant_identifier_names
    required this.private_gene,
  });


  factory AreaGet.fromJson(Map<String, dynamic> json){
    String? description;
    if (json['description'] != null) {
      description = json['description'] as String;
    }
    GeneControllerGet? privateGene;
    if (json['private_gene'] != null) {
      privateGene = GeneControllerGet.fromJson(json['private_gene'] as Map<String, dynamic>);
    }

    return AreaGet(
      name: json['name'] as String,
      id: json['id'] as int,
      description: description,
      //ignore: non_constant_identifier_names
      animals_count: json['animals_count'] as int,
      //ignore: non_constant_identifier_names
      gene_activated: json['gene_activated'] as bool,
      //ignore: non_constant_identifier_names
      private_gene: privateGene,
    );
  }
}