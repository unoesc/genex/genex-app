import 'package:inbov/models/semi_model.dart';

class GetReport {
  int cows;
  // ignore: non_constant_identifier_names
  DateTime min_date;
  int repetitions;
  int inseminations;
  // ignore: non_constant_identifier_names
  int missed_fertilizations;
  int pregnants;
  // ignore: non_constant_identifier_names
  int cow_drys;
  // ignore: non_constant_identifier_names
  int cow_lactations;
  // ignore: non_constant_identifier_names
  int cow_fattening;
  // ignore: non_constant_identifier_names
  List<SemisGet> used_semis;

  GetReport({
    required this.cows,
    // ignore: non_constant_identifier_names
    required this.min_date,
    required this.repetitions,
    required this.inseminations,
    // ignore: non_constant_identifier_names
    required this.missed_fertilizations,
    required this.pregnants,
    // ignore: non_constant_identifier_names
    required this.cow_drys,
    // ignore: non_constant_identifier_names
    required this.cow_lactations,
    // ignore: non_constant_identifier_names
    required this.cow_fattening,
    // ignore: non_constant_identifier_names
    required this.used_semis,
  });

  factory GetReport.fromJson(Map<String, dynamic> json) => GetReport(
    cows: json["cows"] as int,
    min_date: DateTime.parse(json["min_date"] as String),
    repetitions: json["repetitions"] as int,
    inseminations: json["inseminations"] as int,
    missed_fertilizations: json["missed_fertilizations"] as int,
    pregnants: json["pregnants"] as int,
    cow_drys: json["cow_drys"] as int,
    cow_lactations: json["cow_lactations"] as int,
    cow_fattening: json["cow_fattening"] as int,
    used_semis: List<SemisGet>.from(json["used_semis"].map((x) => SemisGet.fromJson(x))),
  );
}