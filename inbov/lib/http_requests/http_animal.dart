import 'dart:convert';
import '../configs.dart';
import 'package:dio/dio.dart';
import '../models/animal_model.dart';
import '../http_requests/http_utils.dart';

Future<void> createAnimal(Map<String, dynamic> data) async {
  String json = jsonEncode(data);
  await requestWrapper(
    () => api.dio.post("/animal", data: json),
    errorReference: "createAnimal"
  );
}

Future<GetAnimals> getAnimals(Map<String, dynamic> data) async {
  String json = jsonEncode(data);
  Response<dynamic> res = await requestWrapper(
    () => api.dio.patch("/animals/", data: json),
    errorReference: "getAnimals"
  );
  return GetAnimals.fromJson(res.data);
}


Future<void> deleteAnimal(int areaId, String animalID) async {
  await requestWrapper(
    () => api.dio.delete("/animal/$areaId/$animalID"),
    errorReference: "deleteAnimal"
  );
}

