import 'package:dio/dio.dart';


Future<T> requestWrapper<T>(Function request, {String? errorReference}) async {
  try {
    return await request();
  } catch (e) {
    print(e);
    print("Error Reference: ${errorReference ?? 'unknown'}");
    if (e is DioError) {
      print(e.message);
      throw e.response?.statusCode ?? 500;
    }
    throw 500;
  }
}
