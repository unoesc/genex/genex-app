import 'dart:convert';
import '../configs.dart';
import 'package:dio/dio.dart';
import '../models/animal_history_model.dart';
import '../http_requests/http_utils.dart';

Future<GetHistories> getHistory(Map<String, dynamic> filters) async {
  String json = jsonEncode(filters);
  Response<dynamic> res = await requestWrapper(
    () => api.dio.patch("/history-get/", data: json),
    errorReference: "getHistory"
  );
  return GetHistories.fromJson(res.data);
}

Future<void> updateHistory(Map<String, dynamic> data) async {
  String json = jsonEncode(data);
  await requestWrapper(
    () => api.dio.patch("/history-edit/", data: json),
    errorReference: "updateHistory"
  );
}

Future<List<String>> getStatusOptions(String animalId, int areaId) async {
  Response<dynamic> res = await requestWrapper(
    () => api.dio.get("/history-options/$animalId/$areaId"),
    errorReference: "getStatusOptions"
  );
  List<dynamic> response = res.data;
  List<String> statusOptions = response.map((e) => e as String).toList();
  return statusOptions;
}

Future<List<HistoryEvents>> getHistoryEvents(int areaId) async {
  Response<dynamic> res = await requestWrapper(
    () => api.dio.get("/history-events/$areaId"),
    errorReference: "getHistoryEvents"
  );
  List<dynamic> response = res.data;
  List<HistoryEvents> historyEvents = response.map((e) => HistoryEvents.fromJson(e as Map<String, dynamic>)).toList();
  return historyEvents;
}