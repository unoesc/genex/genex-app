import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';


class Cookie {
  final String name, value;
  Cookie(this.name, this.value);
}


class CookieInterceptor extends Interceptor {
  final FlutterSecureStorage _storage = const FlutterSecureStorage();
  String? __activeCookies;
  final String __sep = '; ';
  final String __key = 'cookies';

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {
    options.extra['withCredentials'] = true;
    __activeCookies ??= await _storage.read(key: __key);
    if (__activeCookies != null && __activeCookies!.isNotEmpty) {
      options.headers['cookie'] = __activeCookies;
    }
    handler.next(options);
  }

  @override
  void onResponse(Response<dynamic> response, ResponseInterceptorHandler handler) async {
    try {
      await _saveCookies(response);
      handler.next(response);
    } catch (e, stack) {
      DioError err = DioError(
        requestOptions: response.requestOptions,
        error: e,
      );
      err.stackTrace = stack;
      handler.reject(err, true);
    }
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) async {
    if (err.response == null) {
      handler.next(err);
    } else {
      Response<dynamic> response = err.response!;
      try {
        await _saveCookies(response);
        handler.next(err);
      } catch (e, stack) {
        DioError err = DioError(
          requestOptions: response.requestOptions,
          error: e,
        );
        err.stackTrace = stack;
        handler.next(err);
      }
    }
  }

  Cookie __cookieFromString(String txt) {
    List<String> values = txt.split('=');
    return Cookie(values[0], values.sublist(1).join('='));
  }

  Future<void> _saveCookies(Response<dynamic> response) async {
    List<String> cookies = <String>[];
    for (String key in <String>['set-cookie', 'Set-Cookie']){
      cookies.addAll(response.headers[key] ?? <String>[]);
    }
    if (cookies.isEmpty){
      return;
    }
    __activeCookies = cookies.join(__sep);
    await _storage.write(key: __key, value: cookies.join(__sep));
  }

  Future<List<Cookie>> getCookies() async {
    __activeCookies ??= await _storage.read(key: __key);
    return __activeCookies?.split(__sep).map(__cookieFromString).toList() ?? <Cookie>[];
  }

  Future<void> deleteCookie() async {
    await _storage.delete(key: __key);
    __activeCookies = "";
  }

}
