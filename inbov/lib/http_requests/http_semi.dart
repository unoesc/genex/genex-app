import 'dart:convert';
import '../configs.dart';
import 'package:dio/dio.dart';
import '../models/semi_model.dart';
import '../http_requests/http_utils.dart';


Future<List<SemisGet>> getSemis(int areaId) async {
  Response<dynamic> res = await requestWrapper(
    () => api.dio.get("/semis/$areaId"),
    errorReference: "getSemis"
  );
  List<dynamic> semis = res.data;
  return semis.map((semi) => SemisGet.fromJson(semi)).toList();
}


Future<void> createSemi(Map<String, dynamic> data) async {
  String json = jsonEncode(data);
  await requestWrapper(
    () => api.dio.post("/semis", data: json),
    errorReference: "createSemi"
  );
}