import '../configs.dart';
import 'package:dio/dio.dart';
import '../models/report_model.dart';
import '../http_requests/http_utils.dart';

Future<GetReport> getReports(int areaId, String month) async {
  Response<dynamic> res = await requestWrapper(
    () => api.dio.get("/reports/$areaId/$month"),
    errorReference: "getReports"
  );
  return GetReport.fromJson(res.data);
}