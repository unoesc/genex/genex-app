import 'dart:convert';
import '../configs.dart';
import 'package:dio/dio.dart';
import '../models/user_model.dart';
import '../http_requests/http_utils.dart';


Future<LoginUser> loginUser(Map<String, dynamic> data) async {
  String json = jsonEncode(data);
  Response<dynamic> res = await requestWrapper(
    () => api.dio.put("/login", data: json),
    errorReference: "loginUser"
  );
  return LoginUser.fromJson(res.data);
}


Future<LoginUser> createUser(Map<String, dynamic> data) async {
  String json = jsonEncode(data);
  Response<dynamic> res = await requestWrapper(
    () => api.dio.post("/create-user", data: json),
    errorReference: "createUser"
  );
  return LoginUser.fromJson(res.data);
}


Future<GetUser> getUser() async {
  Response<dynamic> res = await requestWrapper(
    () => api.dio.get("/user"),
    errorReference: "getUser"
  );
  return GetUser.fromJson(res.data);
}


Future<void> updateUser(Map<String, dynamic> data) async {
  String json = jsonEncode(data);
  await requestWrapper(
    () => api.dio.patch("/user", data: json),
    errorReference: "updateUser"
  );
}


