import 'dart:convert';
import '../configs.dart';
import 'package:dio/dio.dart';
import '../models/area_model.dart';
import '../http_requests/http_utils.dart';


Future<AreaGet> getArea(int areaId) async {
  Response<dynamic> res = await requestWrapper(
    () => api.dio.get("/area/$areaId"),
    errorReference: "getArea"
  );
  return AreaGet.fromJson(res.data);
}


Future<List<AreaGet>> getAreas() async {
  Response<dynamic> res = await requestWrapper(
    () => api.dio.get("/user-areas"),
    errorReference: "getAreas"
  );
  if (res.data == null) return [];
  List<dynamic> areas = res.data;
  return areas.map((area) => AreaGet.fromJson(area)).toList();
}


Future<void> createArea(Map<String, dynamic> data) async {
  String json = jsonEncode(data);
  await requestWrapper(
    () => api.dio.post("/area", data: json),
    errorReference: "createArea"
  );
}


Future<void> updateArea(Map<String, dynamic> data) async {
  String json = jsonEncode(data);
  await requestWrapper(
    () => api.dio.patch("/area", data: json),
    errorReference: "updateArea"
  );
}


Future<void> deleteArea(int areaId) async {
  await requestWrapper(
    () => api.dio.delete("/area/$areaId"),
    errorReference: "deleteArea"
  );
}

