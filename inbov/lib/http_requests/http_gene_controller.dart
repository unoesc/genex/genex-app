import 'dart:convert';
import '../configs.dart';
import 'package:dio/dio.dart';
import '../models/gene_controller_model.dart';
import '../http_requests/http_utils.dart';

Future<void> updateGeneController(Map<String, dynamic> data) async {
  String json = jsonEncode(data);
  await requestWrapper(
    () => api.dio.patch("/gene-controller", data: json),
    errorReference: "updateGeneController"
  );
}


Future<GeneControllerGet> getGeneController(int areaId) async {
  Response<dynamic> res = await requestWrapper(
    () => api.dio.get("/gene-controller/$areaId"),
    errorReference: "getGeneController"
  );
  return GeneControllerGet.fromJson(res.data);
}
