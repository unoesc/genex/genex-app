import 'dart:convert';
import 'package:inbov/models/user_model.dart';
import 'package:dio/dio.dart';
import '../configs.dart';
import 'api_cookie_interceptor.dart';

class API {
  Dio dio = Dio(BaseOptions(
    baseUrl: BASE_URL,
    connectTimeout: 60000,
    receiveTimeout: 60000,
  ));

  final CookieInterceptor _cookies = CookieInterceptor();

  API() {
    dio.interceptors.add(_cookies);
  }

  Future<bool> isLogged() async {
    return await getUserData() != null;
  }

  Future<void> logout() async {
    await _cookies.deleteCookie();
  }

  Codec<String, String> stringToBase64 = utf8.fuse(base64Url);
  Future<LoginUser?> getUserData() async {
    List<Cookie> cookies = await _cookies.getCookies();
    for (Cookie cookie in cookies){
      if (cookie.name == 'token'){
        List<String> data = cookie.value.split('.');
        if (data.length == 3){
          String payload = data[1];
          while (payload.length % 4 != 0){
            payload += "=";
          }
          String encoded = stringToBase64.decode(payload);
          Map<String, dynamic> userData = json.decode(encoded);
          return LoginUser.fromJson(userData);
        }
      }
    }
    return null;
  }
}
