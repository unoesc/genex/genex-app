import 'package:inbov/configs.dart';
import 'package:inbov/models/animal_model.dart';
import 'package:inbov/widgets/timeline.dart';
import 'package:flutter/material.dart';
// ignore: library_prefixes
import '../http_requests/http_animal_history.dart' as dioAnimalHistory;
import '../models/animal_history_model.dart';
import '../models/area_model.dart';
import '../widgets/alter_history_page.dart';
import '../widgets/date_picker.dart';
import '../widgets/unexpected_error.dart';

class HistoryPage extends StatefulWidget {
  final GetAnimal animal;
  final AreaGet area;
  const HistoryPage({
    Key? key,
    required this.area,
    required this.animal,
  }) : super(key: key);
  @override
  State<HistoryPage> createState() => _HistoryPageState();
}

class _HistoryPageState extends State<HistoryPage> {
  List<GetHistory> histories = [];
  bool loading = false;
  bool error = false;

  List<String> statusOptions = [];
  String selectedStatus = 'Todos';
  DateTime? selectedDate;
  late DateTime minDate;


  @override
  void initState() {
    super.initState();
    getHistories();
  }

  Future<void> getHistories() async{
    setState(() {
      loading = true;
      error = false;
    });
    try{
      Map<String, dynamic> filters = {
        'animal_id': widget.animal.id,
        'start_date': selectedDate != null ? selectedDate.toString().split(' ')[0] : null,
        'status': selectedStatus != 'Todos' ? selectedStatus : null,
      };
      GetHistories histories = await dioAnimalHistory.getHistory(filters);
      setState(() {
        this.histories = histories.histories;
        selectedDate = histories.initial_date;
        minDate = histories.initial_date;
        statusOptions = histories.filters;
        statusOptions.insert(0, 'Todos');
      });
    }catch(e, stack){
      print(e);
      print(stack);
      setState(() {
        error = true;
      });
    }finally{
      setState(() {
        loading = false;
      });
    }
  }


  Widget statusDropDown() {
    return Material(
      borderRadius: BorderRadius.circular(10),
      elevation: 0,
      color: Colors.white,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: secondary,
            width: 1,
          ),
        ),
        child:  DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              value: selectedStatus,
              onChanged: (String? newValue) async {
                if (newValue != null) {
                  setState(() {
                    selectedStatus = newValue;
                  });
                  await getHistories();
                }
              },
              items: statusOptions.map((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value, style: TextStyle(
                    color: switchAnimalColor(value),
                  ),
                  ),
                );
              }).toList(),
            )
        ),
      )
    );
  }


  Widget datePicker() {
    return InkWell(
      onTap: () async {
        DateTime? date = await DatePicker.show(
          context,
          selectedDate!,
          minDate,
          DateTime.now(),
        );
        if (date != null) {
          setState(() {
            selectedDate = date;
          });
          await getHistories();
        }
      },
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: secondary,
            width: 1,
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            const Icon(
              Icons.calendar_today,
              color: Colors.black,
            ),
            const SizedBox(width: 10),
            Text(
              selectedDate != null ? dateFormat(selectedDate!) : 'Selecione uma data',
              style: const TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
            ),
            const SizedBox(width: 10),
            const Icon(
              Icons.arrow_drop_down,
              color: Colors.black,
            ),
          ],
        ),
      ),
    );
  }


  Widget timeLineItem(GetHistory history) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          color: switchAnimalColor(history.status),
          width: 3,
        ),
      ),
      child: Column(
        children: [
          Text(
            dateFormat(history.created_datetime, withTime: true),
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: switchAnimalColor(history.status),
            ),
          ),
          const SizedBox(height: 10),
          Row(
            children: [
              const SizedBox(width: 10),
              Text(
                "Status: ${history.status}",
                style: const TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          if(history.private_semi != null)
          ...[
              const Text(
                "Sêmen privado",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                ),
              ),
            const SizedBox(height: 10),
            Row(
              children: [
                const SizedBox(width: 10),
                Text(
                  "Raça: ${history.private_semi!.breed}",
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                const SizedBox(width: 10),
                Text(
                  "Identificador do Touro: ${history.private_semi!.bull_id}",
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
          if(history.public_semi_breed != null)
          ...[
            const Text(
              "Sêmen público",
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                const SizedBox(width: 10),
                Text(
                  "Raça: ${history.public_semi_breed}",
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            const SizedBox(height: 10),
            Row(
              children: [
                const SizedBox(width: 10),
                Text(
                  "Identificador do Touro: ${history.public_semi_bull_id}",
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ]
        ],
      )
    );
  }


  Widget alterHistory() {
    return FloatingActionButton(
      onPressed: () async {
        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => AlterHistoryPage(
              animal: widget.animal,
              area: widget.area,
            ),
          ),
        );
        await getHistories();
      },
      backgroundColor: Colors.transparent,
      elevation: 0,
      foregroundColor: Colors.green.shade800,
      child: Container(
        height: 50,
        width: 50,
        decoration: BoxDecoration(
          color: Colors.green.shade50,
          borderRadius: BorderRadius.circular(50),
          border: Border.all(
            color: Colors.green.shade800,
            width: 1,
          ),
        ),
        child: const Icon(
          Icons.edit,
          size: 30,
        ),
      ),
    );
  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.animal.name != null ? "${widget.animal.name} - ${widget.animal.id}" : widget.animal.id),
      ),
      body: RefreshIndicator(
        onRefresh: getHistories,
        child: (){
          if (loading){
            return Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 10),
                    Text('Carregando as informações...'),
                  ]),
            );
          }else if (error){
            return UnexpectedError(onRefresh: getHistories,);
          }else{
            return SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  const SizedBox(height: 10),
                  const Text(
                    "Histórico",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30
                    ),
                  ),
                  const SizedBox(height: 10),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const SizedBox(width: 42),
                      datePicker(),
                      const Spacer(),
                      statusDropDown(),
                      const SizedBox(width: 5),
                    ],
                  ),
                  const SizedBox(height: 10),
                  const Divider(color: secondary, height: 2,),
                  const SizedBox(height: 10),
                  SizedBox(
                    height: 600,
                    child: TimeLine(
                        lineColor: primary,
                        indicatorColor: secondary,
                        children: histories.map((history) {
                          return timeLineItem(history);
                        }).toList()
                    ),
                  )
                ],
              ),
            );
          }
        }(),
      ),
      floatingActionButton: alterHistory(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}