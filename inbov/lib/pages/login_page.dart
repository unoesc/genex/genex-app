import 'dart:async';
import 'package:inbov/widgets/generic_loading_dialog.dart';
import '../configs.dart';
import '../utils/validators.dart';
import '../widgets/password_field.dart';
import '../widgets/text_field.dart';
import 'package:flutter/material.dart';
// ignore: library_prefixes
import '../http_requests/http_user.dart' as dioUser;
import '../widgets/response_confirmation.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool loading = false;

  Widget _entryFieldEmail() {
    return TextFieldPage(
      controller: emailController,
      loading: loading,
      obscure: false,
      text: 'E-mail *',
      typeInput: TextInputType.emailAddress,
      typeHints: const [AutofillHints.email],
      validate: requiredFieldValidator,
      focus: false,
      outlined: false,
    );
  }


  Widget _entryFieldPassword() {
    return PasswordField(
      signUp: false,
      controller: passwordController,
      loading: loading,
      text: 'Senha *',
      validate: requiredFieldValidator,
    );
  }

  Widget _submitButton() {
    return InkWell(
      onTap: () async {
        if (_formKey.currentState!.validate() && !loading) {
          setState(() {
            loading = true;
          });
          LoadingDialog.show(context, message: 'Entrando...');
          Map<String, dynamic> data = {
            "email": emailController.text,
            "password": passwordController.text
          };
          switcher(code) {
            switch (code) {
              case 404:
                {
                  ResponseConfirmation.show(
                      context, "Email informado não foi encontrado", 404);
                  break;
                }
              case 401:
                {
                  ResponseConfirmation.show(context, "Senha incorreta", 401);
                  break;
                }
              default:
                {
                  ResponseConfirmation.show(
                      context,
                      "Ocorreu um erro interno, por favor tente mais tarde",
                      500);
                }
            }
          }
          try {
            await dioUser.loginUser(data);
            setState(() {
              emailController.clear();
              passwordController.clear();
            });
            //ignore: use_build_context_synchronously
            Navigator.pop(context);
            //ignore: use_build_context_synchronously
            unawaited(Navigator.pushReplacementNamed(context, '/area-area'));
          } catch (e) {
            //ignore: use_build_context_synchronously
            Navigator.pop(context);
            switcher(e);
          } finally {
            setState(() {
              loading = false;
            });
          }
        }
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(8)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black.withAlpha(50),
              offset: const Offset(0, 2),
              blurRadius: 2,
            )
          ],
          color: primary,
        ),
        child: const Text(
          'ENTRAR',
          style: TextStyle(color: Colors.white),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }

  Widget _createAccountLabel() {
    Size size = MediaQuery.of(context).size;
    return TextButton(
      onPressed: () {
        Navigator.pushNamed(context, '/signup');
      },
      child: const Center(
          child: Text('QUERO FAZER MEU CADASTRO',
              style: TextStyle(fontSize: 14, color: primary)),
        ),
    );
  }

  Widget _title() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Image.asset(
          'assets/icon_menor.png',
          width: 100,
        ),
        const SizedBox(width: 10),
        const Text("Login",
            style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)
        ),
      ],
    );
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
        Center(
          child:
            Container(
              width: 400,
              color: Colors.white,
              padding: const EdgeInsets.all(20),
              margin: const EdgeInsets.all(40),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  _title(),
                  const SizedBox(height: 10),
                  AutofillGroup(
                    child: Form(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      key: _formKey,
                      child: AutofillGroup(
                        child: Column(
                          children: <Widget>[
                            _entryFieldEmail(),
                            const SizedBox(height: 5),
                            _entryFieldPassword(),
                          ],
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 10),
                  _submitButton(),
                  const SizedBox(height: 10),
                  _createAccountLabel(),
                ],
              )
          )
      ),
    );
  }

}
