import 'package:date_time_picker/date_time_picker.dart';
import 'package:inbov/widgets/generic_loading_dialog.dart';
import 'package:inbov/widgets/response_confirmation.dart';
import '../models/area_model.dart';
import '../utils/validators.dart';
import '../widgets/text_field.dart';
import '../widgets/unexpected_error.dart';
// ignore: library_prefixes
import '../http_requests/http_area.dart' as dioArea;
import '../configs.dart';
import 'package:flutter/material.dart';

class AddAreaPage extends StatefulWidget {
  final AreaGet? area;
  const AddAreaPage({Key? key, this.area}) : super(key: key);
  @override
  State<AddAreaPage> createState() => _AddAreaPageState();
}

class _AddAreaPageState extends State<AddAreaPage> {
  TextEditingController nameAreaController = TextEditingController();
  TextEditingController descriptionAreaController = TextEditingController();
  int privateGeneController = 0;
  final GlobalKey<FormState> _areaFormKey = GlobalKey<FormState>();
  final GlobalKey<FormState> _geneFormKey = GlobalKey<FormState>();

  TextEditingController nitrogenStatus = TextEditingController();
  TextEditingController nitrogenLossTax = TextEditingController();
  DateTime nitrogenNextRefill = DateTime.now().add(const Duration(days: 1));
  DateTime nitrogenLastRefill = DateTime.now();

  bool loading = false;



  @override
  void initState() {
    super.initState();
    if(widget.area != null){
      nameAreaController.text = widget.area!.name;
      descriptionAreaController.text = widget.area!.description ?? '';
      privateGeneController = widget.area!.gene_activated ? 1 : 0;
      if (widget.area!.gene_activated) {
        nitrogenStatus.text = widget.area!.private_gene!.nitrogen_current_status.toString();
        nitrogenLossTax.text = widget.area!.private_gene!.nitrogen_day_loss_tax.toString();
        nitrogenLastRefill = widget.area!.private_gene!.nitrogen_last_refill ?? DateTime.now();
        nitrogenNextRefill = widget.area!.private_gene!.nitrogen_next_refill ?? DateTime.now().add(const Duration(days: 1));
      }
    }
  }


  Widget _entryField(String title, TextEditingController controller, Function(String)? validate, TextInputType inputType, {String? mask}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 20),
        TextFieldPage(
          controller: controller,
          loading: loading,
          obscure: false,
          text: title,
          validate: validate,
          focus: false,
          outlined: true,
          typeInput: inputType,
          mask: mask,
        ),
      ],
    );
  }


  Widget _geneCheckBox(){
    return Column(
        children: <Widget>[
          const SizedBox(height: 20),
          const Text('A fazenda possui estoque próprio de sêmens?',
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold
              ),
            textAlign: TextAlign.center
          ),
          const SizedBox(height: 10),
          Row(
            children: <Widget>[
              for (int i = 0; i <= 1; i++)
                Row(
                  children: <Widget>[
                    Radio(
                      value: i,
                      groupValue: privateGeneController,
                      onChanged: (int? value) {
                        setState(() {
                          if (value != null) {
                            privateGeneController = value;
                          }
                        });
                      },
                    ),
                    Text(
                      i == 1 ? 'Sim' : 'Não',
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ]
                )
            ],
        ),
      ],
    );
  }




  Widget areaForm() {
    return AutofillGroup(
        child: Form(
          key: _areaFormKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Container(
            color: Colors.white,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: <Widget>[
                _entryField("Nome da Fazenda *", nameAreaController, requiredFieldValidator, TextInputType.text),
                _entryField("Descrição", descriptionAreaController, null, TextInputType.text),
                _geneCheckBox(),
              ],
            ),
          ),
        )
    );
  }

  Widget _calendarPicker(String title, DateTime? pickedDate, DateTime firstDate, DateTime lastDate, DateTime date, Function(String) onDateChanged) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const SizedBox(height: 20),
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: radiusBorder,
              ),
              child:
                DateTimePicker(
                  type: DateTimePickerType.date,
                  dateMask: 'dd/MM/yyyy',
                  initialDate: pickedDate,
                  initialValue: date.toString(),
                  initialDatePickerMode: DatePickerMode.day,
                  firstDate: firstDate,
                  lastDate: lastDate,
                  style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                  ),
                  calendarTitle: 'SELECIONE A DATA',
                  decoration: InputDecoration(
                    border: const UnderlineInputBorder(),
                    labelText: title,
                    contentPadding: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical: 10,
                    ),
                    hintStyle: TextStyle(
                      color: Colors.grey[700],
                    ),
                    suffixIcon: Icon(
                      Icons.calendar_today,
                      color: Colors.grey[700],
                    ),
                    labelStyle: const TextStyle(
                      color: secondary,
                      fontSize: 18,
                    ),
                    focusedBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: secondary,
                          width: 1,
                        )),
                    enabledBorder: const UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: secondary,
                          width: 1,
                        )),
                  ),
                  onChanged: onDateChanged,
                )
            ),
        ],
    );
  }



  Widget _geneForm() {
    return AutofillGroup(
        child: Form(
          key: _geneFormKey,
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Container(
            color: Colors.white,
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: <Widget>[
                const Text(
                    "Dados do estóque de sêmens",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold
                    ),
                  textAlign: TextAlign.center,
                ),
                _entryField("Quantidade de Nítrogênio (Litros) *", nitrogenStatus, requiredFieldValidator, TextInputType.number, mask: '##.##'),
                _entryField("Taxa de perda (Litros/dia) *", nitrogenLossTax, requiredFieldValidator, TextInputType.number, mask: '##.##'),
                _calendarPicker(
                    "Último abastecimento de nitrogênio",
                    nitrogenLastRefill,
                    DateTime.now().subtract(const Duration(days: 365)),
                    DateTime.now(),
                    nitrogenLastRefill,
                    (String value) {
                      setState(() {
                        nitrogenLastRefill = DateTime.parse(value);
                      });
                    }
                ),
                _calendarPicker(
                    "Próximo abastecimento de nitrogênio",
                    nitrogenNextRefill,
                    DateTime.now().add(const Duration(days: 1)),
                    DateTime.now().add(const Duration(days: 365)),
                    nitrogenNextRefill,
                    (String value) {
                      setState(() {
                        nitrogenNextRefill = DateTime.parse(value);
                      });
                    }
                ),
              ],
            ),
          ),
        )
    );
  }


  Future<bool> isValid() async {
    if (_areaFormKey.currentState!.validate() && !loading) {
      if (privateGeneController == 1) {
        if (_geneFormKey.currentState!.validate()) {
          return true;
        }
        else {
          return false;
        }
      } else {
        return true;
      }
    }
    else {
      return false;
    }
  }


  Future<void> submitFunction() async {
    if (await isValid()) {
      setState(() {
        loading = true;
      });
      LoadingDialog.show(context, message: "Salvando...");
      try{
        Map<String, dynamic> data = {
          "name": nameAreaController.text,
          "description": descriptionAreaController.text != '' ? descriptionAreaController.text : null,
          "private_gene": privateGeneController == 0 ? null
              : {
            "nitrogen_LT_status": double.parse(nitrogenStatus.text),
            "nitrogen_day_loss_tax": double.parse(nitrogenLossTax.text),
            "nitrogen_next_refill": nitrogenNextRefill.toString().split(' ')[0],
            "nitrogen_last_refill": nitrogenLastRefill.toString().split(' ')[0],
          },
        };
        await dioArea.createArea(data);
        setState(() {
          nameAreaController.clear();
          descriptionAreaController.clear();
          privateGeneController = 0;
          nitrogenStatus.clear();
          nitrogenLossTax.clear();
          nitrogenNextRefill = DateTime.now().add(const Duration(days: 1));
          nitrogenLastRefill = DateTime.now();
        });
        // ignore: use_build_context_synchronously
        Navigator.pop(context);
        // ignore: use_build_context_synchronously
        Navigator.pop(context);
      }catch(e, stack) {
        Navigator.pop(context);
        print(e);
        print(stack);
        ResponseConfirmation.show(context, "Ocorreu um erro interno, por favor tente mais tarde", 500);
      }finally{
        setState(() {
          loading = false;
        });
      }
    }
  }

  Future<void> updateFunction() async {
    if (await isValid()) {
      setState(() {
        loading = true;
      });
      LoadingDialog.show(context, message: "Salvando...");
      try{
        Map<String, dynamic> data = {
          "id": widget.area!.id,
          "name": nameAreaController.text,
          "description": descriptionAreaController.text != '' ? descriptionAreaController.text : null,
          "private_gene": privateGeneController == 0 ? null
              : {
            "nitrogen_LT_status": double.parse(nitrogenStatus.text),
            "nitrogen_day_loss_tax": double.parse(nitrogenLossTax.text),
            "nitrogen_next_refill": nitrogenNextRefill.toString().split(' ')[0],
            "nitrogen_last_refill": nitrogenLastRefill.toString().split(' ')[0],
          },
        };
        await dioArea.updateArea(data);
        // ignore: use_build_context_synchronously
        Navigator.pop(context);
        // ignore: use_build_context_synchronously
        await ResponseConfirmation.show(context, "Dados alterados com sucesso", 200);
        // ignore: use_build_context_synchronously
        Navigator.pop(context);
      }catch(e, stack) {
        Navigator.pop(context);
        print(e);
        print(stack);
        ResponseConfirmation.show(context, "Ocorreu um erro interno, por favor tente mais tarde", 500);
      }finally{
        setState(() {
          loading = false;
        });
      }
    }
  }




  Widget _submitButton() {
    return InkWell(
      onTap: () async {
        if (widget.area != null) {
          await updateFunction();
        } else {
          await submitFunction();
        }
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: const LinearGradient(
            colors: [
              Color(0xFF4caf50),
              Color(0xFF8bc34a),
            ],
          ),
        ),
        child: const Text(
          'Salvar',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }




  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.area != null ? "Editar Fazenda" :'Adicionar Fazenda',
          style: const TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold
          ),
        ),
        centerTitle: true,
      ),
      body: SizedBox(
        height: height,
        child: SingleChildScrollView(
          child: Center(
            child: Container(
              padding: const EdgeInsets.only(
                  top: 10, bottom: 30, left: 20, right: 20),
              child: Column(
                children: [
                  const SizedBox(height: 20),
                  areaForm(),
                  const SizedBox(height: 20),
                  if (privateGeneController == 1)
                    ...[
                      _geneForm(),
                    const SizedBox(height: 20),
                  ],
                  if (privateGeneController == 1 && widget.area != null && widget.area!.private_gene != null)
                    ...[
                      const Text(
                          "Seu código para ativação de sensor de nitrogênio:",
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      const SizedBox(height: 10),
                      Text(
                        widget.area!.private_gene!.sensor_token,
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: secondary
                        ),
                      ),
                      const SizedBox(height: 20),
                    ],
                  _submitButton(),
                  const SizedBox(height: 20),
                ],
              )
            )
          ),
        )
      ),
    );
  }
}