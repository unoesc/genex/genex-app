import 'package:flutter/material.dart';
import 'package:inbov/widgets/add_semi_dialog.dart';
import 'package:inbov/widgets/alter_history_page.dart';
import 'package:inbov/widgets/fill_nitrogen_dialog.dart';
import '../../configs.dart';
// ignore: library_prefixes
import '../../http_requests/http_animal_history.dart' as dioHistory;
import '../../models/animal_history_model.dart';
import '../../models/area_model.dart';
import '../../widgets/unexpected_error.dart';

class EventsPage extends StatefulWidget {
  final AreaGet area;
  const EventsPage({
    Key? key,
    required this.area,
  }) : super(key: key);
  @override
  State<StatefulWidget> createState() => _EventsPageState();
}

class _EventsPageState extends State<EventsPage> {
  bool loading = false;
  bool error = false;

  List<HistoryEvents> events = [];

  @override
  void initState() {
    super.initState();
    getEvents();
  }

  Future<void> getEvents() async {
    setState(() {
      loading = true;
      error = false;
    });
    try {
      List<HistoryEvents> events = await dioHistory.getHistoryEvents(widget.area.id);
      setState(() {
        this.events = events;
      });
    } catch (e, stacktrace) {
      print(e);
      print(stacktrace);
      setState(() {
        error = true;
      });
    }finally{
      setState(() {
        loading = false;
      });
    }
  }


  Color switchEventColor(String type){
    switch(type){
      case 'nitrogen':
        return Colors.blue;
      case 'semi':
        return Colors.yellow.shade700;
      case 'animal':
        return Colors.green;
      default:
        return Colors.grey;
    }
  }


  Future<void> decidePage(HistoryEvents event) async {
    if (event.type == 'nitrogen'){
      await FillNitrogenDialog.show(context, widget.area);
    }
    else if (event.type == 'semi'){
      await AddSemiDialog.show(context, widget.area);
    }
    else if (event.type == 'animal'){
      await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => AlterHistoryPage(
            animal: event.animal!,
            area: widget.area,
          ),
        ),
      );
    }
    await getEvents();
  }


  Widget eventCard(HistoryEvents event){
    return InkWell(
      onTap: () async {
        await decidePage(event);
      },
      child: Container(
        padding: const EdgeInsets.all(15),
        margin: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: switchEventColor(event.type),
            width: 3,
          ),
        ),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                event.title,
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 10),
              if (event.occurrence_date != null)
                Text(
                  "Data: ${dateFormat(event.occurrence_date!)}",
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
              const SizedBox(height: 10),
              if (event.text != null)
                Text(
                  event.text!,
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.justify,
                ),
              const SizedBox(height: 10),
              if (event.animal != null)
                Text(
                  "Animal: ${event.animal!.id}",
                  style: const TextStyle(
                    fontSize: 16,
                  ),
                ),
            ],
          ),
        ),
    );
  }




  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: getEvents,
        child: (){
          if(loading){
            return Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 10),
                    Text('Carregando as informações...'),
                  ]),
            );
          }else if(error){
            return UnexpectedError(onRefresh: getEvents);
          }else if(events.isEmpty) {
            return SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(height: 10),
                    const Text('Eventos próximos', style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 30),),
                    const SizedBox(height: 10),
                    const Divider(color: secondary, height: 2,),
                    SizedBox(height: height/2 - height/4),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const <Widget>[
                        Icon(
                          Icons.check_circle,
                          color: Colors.green,
                          size: 50,
                        ),
                        SizedBox(height: 20),
                        Text('Nenhum evento próximo',
                            style: TextStyle(fontSize: 20, color: Colors
                                .black)),
                      ],
                    ),
                    SizedBox(height: height/2 - height/10),
                  ]
              ),
            );
          }else{
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(height: 10),
                  const Text('Eventos próximos', style: TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 30),),
                  const SizedBox(height: 10),
                  const Divider(color: secondary, height: 2,),
                  Container(
                    height: height - 190,
                    alignment: Alignment.center,
                    width: width - 50,
                    child: ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        children: <Widget>[
                          for (HistoryEvents event in events)
                            eventCard(event),
                        ]
                    ),
                  )
                ],
              ),
            );
          }
        }(),
      )
    );
  }
}