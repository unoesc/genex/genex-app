import 'package:flutter/material.dart';
import '../../configs.dart';
//ignore: library_prefixes
import '../../http_requests/http_animal.dart' as dioAnimal;
import '../../models/animal_model.dart';
import '../../models/area_model.dart';
import '../../widgets/add_animal_dialog.dart';
import '../../widgets/confirm_dialog.dart';
import '../../widgets/generic_loading_dialog.dart';
import '../../widgets/response_confirmation.dart';
import '../../widgets/text_field.dart';
import '../../widgets/unexpected_error.dart';
import '../history_page.dart';


class AnimalPage extends StatefulWidget {
  final AreaGet area;
  const AnimalPage({
    Key? key,
    required this.area,
  }) : super(key: key);
  @override
  State<StatefulWidget> createState() => _AnimalPageState();
}

class _AnimalPageState extends State<AnimalPage> {
  TextEditingController searchController = TextEditingController();
  bool showingClear = false;

  List<GetAnimal> animals = [];
  bool loading = false;
  bool error = false;
  bool requestLoading = false;
  String currentStatus = "Todos";
  String? currentFilter;
  List<String> status = [];
  String lastSearch = '';



  @override
  void initState() {
    super.initState();
    getAnimals();
  }

  Future<void> getAnimals() async{
    if (currentFilter != lastSearch && currentFilter != null) {
      setState(() {
        lastSearch = currentFilter!;
      });
    }
    if (currentFilter == '') {
      setState(() {
        currentFilter = null;
        lastSearch = '';
      });
    }

    setState(() {
      loading = true;
      error = false;
    });
    try{
      Map<String, dynamic> filters ={
        "area_id": widget.area.id,
        "current_status": currentStatus == "Todos" ? null : currentStatus,
        "text": currentFilter,
      };
      GetAnimals animals = await dioAnimal.getAnimals(filters);
      setState(() {
        this.animals = animals.animals;
        status = animals.filters;
        status.insert(0, "Todos");
      });
    }catch(e, stack){
      print(e);
      print(stack);
      setState(() {
        error = true;
      });
    }finally{
      setState(() {
        loading = false;
      });
    }
  }


  Widget statusDropDown() {
    return Material(
        borderRadius: BorderRadius.circular(10),
        elevation: 0,
        color: Colors.white,
        child: Container(
          width: MediaQuery.of(context).size.width * 0.9,
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
              color: secondary,
              width: 1,
            ),
          ),
          child:  DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                value: currentStatus,
                onChanged: (String? newValue) async {
                  if (newValue != null) {
                    setState(() {
                      currentStatus = newValue;
                    });
                    await getAnimals();
                  }
                },
                items: status.map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value, style: TextStyle(
                      color: switchAnimalColor(value),
                    ),
                    ),
                  );
                }).toList(),
              )
          ),
        )
    );
  }


  Widget search() {
    return SizedBox(
        child: Column(
        children: [
            Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(width: 10,),
              Expanded(
                flex: 6,
                child: TextFieldPage(
                    controller: searchController,
                    loading: loading,
                    focus: false,
                    obscure: false,
                    text: 'Pesquisar',
                    onChanged: (String? value) {
                      if (value != null && value.isNotEmpty) {
                        if (!showingClear) {
                          setState(() {
                            showingClear = true;
                          });
                        }
                      } else if (showingClear) {
                        setState(() {
                          showingClear = false;
                        });
                      }
                    },
                    suffixIcon: !showingClear ? null : IconButton(
                      icon: const Icon(Icons.clear, color: primary),
                      onPressed: () async {
                        setState(() {
                          showingClear = false;
                          currentFilter = '';
                        });
                        FocusScope.of(context).unfocus();
                        searchController.clear();
                        await getAnimals();
                      },
                    ),
                    outlined: true,
                  ),
              ),
              const SizedBox(width: 10,),
              Expanded(
                flex: 1,
                child: Material(
                  borderRadius: radiusBorder,
                  color: primary,
                  child: InkWell(
                      child: const SizedBox(
                        height: 55,
                        width: 55,
                        child: Icon(
                          Icons.search,
                          color: Colors.white,
                        ),
                      ),
                      onTap: () async {
                        FocusScope.of(context).unfocus();
                        setState(() {
                          currentFilter = searchController.text;
                        });
                        await getAnimals();
                      }
                  ),
                ),
              ),
              const SizedBox(width: 10,),
            ],
          ),
          statusDropDown(),
       ],
      ),
    );
  }



  Widget addAnimalButton() {
    return FloatingActionButton(
      onPressed: () async {
        await AddAnimal.show(context, widget.area);
        await getAnimals();
      },
      backgroundColor: Colors.transparent,
      elevation: 0,
      foregroundColor: Colors.green.shade800,
      child: Container(
        height: 50,
        width: 50,
        decoration: BoxDecoration(
          color: Colors.green.shade50,
          borderRadius: BorderRadius.circular(50),
          border: Border.all(
            color: Colors.green.shade800,
            width: 1,
          ),
        ),
        child: const Icon(
          Icons.add,
          size: 30,
        ),
      ),
    );
  }




  Widget animalCard(GetAnimal animal, int index) {
    return InkWell(
      onTap: () async {
        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => HistoryPage(animal: animal, area: widget.area),
          ),
        ).then((value) => getAnimals());
      },
      onLongPress: () async{
        if (await ConfirmDialog.show(context, 'Deseja excluir o animal ${animal.id}?',)) {
          setState(() {
            loading = true;
          });
          LoadingDialog.show(context, message: 'Excluindo animal ${animal.id}');
          try{
            await dioAnimal.deleteAnimal(widget.area.id, animal.id);
            //ignore: use_build_context_synchronously
            Navigator.pop(context);
          }catch(e, stack){
            //ignore: use_build_context_synchronously
            Navigator.pop(context);
            print(e);
            print(stack);
            ResponseConfirmation.show(context, 'Erro ao excluir animal ${animal.id}', 500);
          }finally{
            setState(() {
              loading = false;
            });
          }
          await getAnimals();
        }
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 10),
        width: 400,
        padding: const EdgeInsets.all(5),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: switchAnimalColor(animal.current_status),
            width: 5,
          ),
        ),
        child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
               Expanded(
                  child: SizedBox(width: 10,
                    child: Text(
                        (index + 1).toString(),
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: switchAnimalColor(animal.current_status),
                      ),
                    )
                  )
              ),
              Expanded(
                flex: 7,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "${animal.name != null ? "${animal.name} - " : ""}${animal.id}",
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                        animal.current_status,
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: switchAnimalColor(animal.current_status),
                        ),
                    ),
                  ],
                )
              ),
              const Expanded(
                  flex: 1,
                  child: Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: Colors.black,
                  )
              ),
            ],
        )
      )
    );
  }



  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: getAnimals,
        child: (){
          if(loading){
            return Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 10),
                    Text('Carregando as informações...'),
                  ]),
            );
          }else if(error){
            return UnexpectedError(onRefresh: getAnimals);
          }else if(animals.isEmpty) {
            return SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(height: 10),
                    const Text('Animais', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
                    const SizedBox(height: 10),
                    search(),
                    const SizedBox(height: 10),
                    const Divider(color: secondary, height: 2,),
                    const SizedBox(height: 200),
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const <Widget>[
                          Icon(
                            Icons.error_outline,
                            color: Colors.red,
                            size: 50,
                          ),
                          SizedBox(height: 20),
                          Text('Nenhum animal encontrado', style: TextStyle(fontSize: 20, color: Colors.black)),
                        ],
                      ),
                    const SizedBox(height: 200),
                  ]
              ),
            );
          }else{
            return SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(height: 10),
                  const Text('Animais', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
                  const SizedBox(height: 10),
                  search(),
                  const SizedBox(height: 10),
                  const Divider(color: secondary, height: 2,),
                  const SizedBox(height: 10),
                  Container(
                    height: height - 330,
                    alignment: Alignment.center,
                    width: width - 50,
                    child: ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        children: <Widget>[
                          for (int i = 0; i < animals.length; i++)
                            animalCard(animals[i], i),
                        ]
                    ),
                  )
                ]
              ),
            );
          }
        }(),
      ),
      floatingActionButton: addAnimalButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}