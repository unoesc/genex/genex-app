import 'package:floating_action_bubble/floating_action_bubble.dart';
import 'package:flutter/material.dart';
import 'package:inbov/configs.dart';
import 'package:inbov/models/gene_controller_model.dart';
import 'package:inbov/widgets/add_semi_dialog.dart';
import 'package:inbov/widgets/fill_nitrogen_dialog.dart';
import '../../models/area_model.dart';
import '../../configs.dart';
//ignore: library_prefixes
import '../../http_requests/http_semi.dart' as dioSemi;
//ignore: library_prefixes
import '../../http_requests/http_gene_controller.dart' as dioGene;
import '../../models/semi_model.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

import '../../widgets/unexpected_error.dart';

class ChartData {
  ChartData(this.x, this.y, this.color);
  final String x;
  final int y;
  final Color color;
}


class SemisPage extends StatefulWidget {
  final AreaGet area;
  const SemisPage({
    Key? key,
    required this.area,
  }) : super(key: key);
  @override
  State<StatefulWidget> createState() => _SemisPageState();
}

class _SemisPageState extends State<SemisPage> with SingleTickerProviderStateMixin{
  List<SemisGet> semis = [];
  bool loading = false;
  bool error = false;
  bool details = false;

  final List<Color> colors = [
    Colors.red, Colors.green, Colors.amber,
    Colors.blue, Colors.purple, Colors.pinkAccent,
    Colors.orange, Colors.teal, Colors.blueAccent,
    Colors.black54, Colors.yellowAccent, Colors.redAccent,
    Colors.lightGreen, Colors.deepPurple, Colors.lightGreen
  ];

  List<ChartData> chartData = [];
  late double nitrogenStatus;
  late DateTime lastUpdate;
  late DateTime nextUpdate;

  late Animation<double> _animation;
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      duration: const Duration(milliseconds: 260),
      vsync: this,
    );
    final curvedAnimation = CurvedAnimation(curve: Curves.easeInOut, parent: _animationController);
    _animation = Tween<double>(begin: 0, end: 1).animate(curvedAnimation);
    getSemis();
  }

  Future<void> getSemis() async{
    setState(() {
      loading = true;
      error = false;
    });
    try{
      List<SemisGet> semis = await dioSemi.getSemis(widget.area.id);
      GeneControllerGet gene = await dioGene.getGeneController(widget.area.id);
      setState(() {
        this.semis = semis;
        nitrogenStatus = gene.nitrogen_current_status!;
        lastUpdate = gene.nitrogen_last_refill!;
        nextUpdate = gene.nitrogen_next_refill!;
      });
      convertSemiToChart();
    }catch(e, stack){
      print(e);
      print(stack);
      setState(() {
        error = true;
      });
    }finally{
      setState(() {
        loading = false;
      });
    }
  }


  void convertSemiToChart(){
    int colorIndex = 0;
    List<ChartData> chartData = [];
    for(SemisGet semi in semis){
      chartData.add(ChartData(semi.group_name, semi.semis.length, colors[colorIndex]));
      if(colorIndex < colors.length - 1){
        colorIndex++;
      }else{
        colorIndex = 0;
      }
    }
    setState(() {
      this.chartData = chartData;
    });
  }


  Widget addSemiButton() {
    return FloatingActionBubble(
      items: [
        Bubble(
            icon: Icons.add,
            iconColor: Colors.white,
            title: "Adicionar Sêmen",
            titleStyle: const TextStyle(fontSize: 16 , color: Colors.white),
            bubbleColor: primary,
            onPress: () async {
              await AddSemiDialog.show(context, widget.area);
              _animationController.reverse();
              await getSemis();
            }
        ),
        Bubble(
          icon: Icons.add,
          iconColor: Colors.white,
          title: "Atualizar Nitrogênio",
          titleStyle: const TextStyle(fontSize: 16 , color: Colors.white),
          bubbleColor: primary,
          onPress: () async {
            await FillNitrogenDialog.show(context, widget.area);
            _animationController.reverse();
            await getSemis();
          }
        )
      ],
      iconColor: Colors.white,
      iconData: Icons.add,
      backGroundColor: primary,
      onPress: () => _animationController.isCompleted
        ? _animationController.reverse()
        : _animationController.forward(),
      animation: _animation,

    );
  }



  Widget _reportItem(String title, String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          title,
          style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        Text(
          value,
          style: const TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }


  Widget detailTable() {
    return Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        margin: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            for (final usedSemi in semis)
              ...[
                const SizedBox(height: 20),
                Row(
                  children: [
                    Text(
                      "${usedSemi.group_name} :",
                      style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const Spacer(),
                    Text(
                      usedSemi.semis.length.toString(),
                      style: const TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ],
                ),
                for (final semi in usedSemi.semis)
                  ...[
                    const Divider(
                      color: Colors.black,
                      thickness: 1,
                    ),
                    _reportItem(
                      "Touro: ${semi.bull_id}",
                      "Data de compra: ${dateFormat(semi.acquisition_datetime)}",
                    ),
                  ],
                const Divider(
                  color: Colors.black,
                  thickness: 1,
                ),
              ],
          ],
        )
    );
  }




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: getSemis,
        child: (){
          if(loading) {
            return Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 10),
                    Text('Carregando as informações...'),
                  ]),
            );
          }else if(error){
            return UnexpectedError(onRefresh: getSemis);
          }else{
            return SingleChildScrollView(
              child:
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const SizedBox(height: 10),
                    const Text('Sêmens', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
                    const Divider(color: secondary, height: 5,),
                    const SizedBox(height: 20),
                    if (semis.isEmpty)
                      SizedBox(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const <Widget>[
                            Icon(
                              Icons.warning_rounded,
                              color: Colors.orange,
                              size: 50,
                            ),
                            SizedBox(height: 20),
                            Text('Nenhum sêmen disponível', style: TextStyle(fontSize: 20, color: Colors.black)),
                          ],
                        ),
                      )
                    else
                      ...[
                        Container(
                          child: SfCircularChart(
                              series: <CircularSeries>[
                                DoughnutSeries<ChartData, String>(
                                    dataSource: chartData,
                                    pointColorMapper:(ChartData data,  _) => data.color,
                                    xValueMapper: (ChartData data, _) => data.x,
                                    yValueMapper: (ChartData data, _) => data.y,
                                    dataLabelSettings: const DataLabelSettings(
                                        isVisible: true,
                                        labelPosition: ChartDataLabelPosition.inside,
                                        textStyle: TextStyle(
                                            color: Colors.white,
                                            fontSize: 15
                                        )
                                    )
                                )
                              ],
                              legend: Legend(
                                  overflowMode: LegendItemOverflowMode.wrap,
                                  position: LegendPosition.bottom,
                                  isVisible: true,
                                  textStyle: const TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                  ),
                                iconHeight: 18,
                                iconWidth: 18,
                              )
                          )
                      ),
                      Center(
                        child: TextButton(
                            onPressed: () {
                              setState(() {
                                if (details) {
                                  details = false;
                                } else {
                                  details = true;
                                }
                              });
                            },
                            child: Text(
                                details ? 'Ocultar detalhes' : 'Ver detalhes',
                                style: const TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold
                                )
                            )
                        )
                      ),
                      if (details)
                        detailTable(),
                    ],
                    const SizedBox(height: 30),
                    const Divider(color: secondary, height: 5,),
                    const SizedBox(height: 30),
                    const Text('Nitrogênio', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
                    const SizedBox(height: 30),
                    Center(
                      child:
                        Text("$nitrogenStatus L", style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            color: nitrogenStatus > 20 ? Colors.green : nitrogenStatus > 15 ? Colors.yellow : Colors.red
                        ),
                      ),
                    ),
                    const SizedBox(height: 30),
                    Container(
                      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                      margin: const EdgeInsets.symmetric(horizontal: 10),
                      child: Column(
                        children: [
                          _reportItem(
                            "Último abastecimento:",
                            dateFormat(lastUpdate)
                          ),
                          const SizedBox(height: 20),
                          _reportItem(
                              "Próximo abastecimento:",
                              dateFormat(nextUpdate)
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 50),
                  ],
              ),
            );
          }
        }(),
      ),
      floatingActionButton: addSemiButton(),
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
    );
  }
}
