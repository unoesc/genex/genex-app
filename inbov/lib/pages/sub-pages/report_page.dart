import 'package:flutter/material.dart';
import 'package:inbov/models/report_model.dart';
// ignore: library_prefixes
import '../../http_requests/http_reports.dart' as dioReport;
import '../../configs.dart';
import '../../models/area_model.dart';
import '../../widgets/date_picker.dart';
import '../../widgets/unexpected_error.dart';

class ReportPage extends StatefulWidget {
  final AreaGet area;
  const ReportPage({
    Key? key,
    required this.area,
  }) : super(key: key);
  @override
  State<ReportPage> createState() => _ReportPageState();
}

class _ReportPageState extends State<ReportPage> {
  DateTime monthSelected = DateTime.now();
  late DateTime initialDate;

  GetReport? report;
  bool loading = false;
  bool error = false;

  @override
  void initState() {
    super.initState();
    getReports();
  }


  Future<void> getReports() async {
    setState(() {
      loading = true;
      error = false;
    });
    try{
      GetReport report = await dioReport.getReports(
        widget.area.id,
        dateToBackEnd(monthSelected),
      );
      setState(() {
        this.report = report;
        initialDate = report.min_date;
      });
    }catch(e, stack){
      print(e);
      print(stack);
      setState(() {
        error = true;
      });
    }finally{
      setState(() {
        loading = false;
      });
    }
  }

  Widget datePicker() {
    return InkWell(
      onTap: () async {
        DateTime? date = await DatePicker.show(
          context,
          monthSelected,
          initialDate,
          DateTime.now(),
          monthMode: true,
        );
        if (date != null) {
          setState(() {
            monthSelected = date;
          });
          await getReports();
        }
      },
      child: Container(
        width: MediaQuery.of(context).size.width * 0.9,
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
        margin: const EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            color: secondary,
            width: 1,
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            const Icon(
              Icons.calendar_today,
              color: Colors.black,
            ),
            const SizedBox(width: 10),
            Text(
              monthFormat(monthSelected, withYear: true),
              style: const TextStyle(
                color: Colors.black,
                fontSize: 16,
              ),
            ),
            const SizedBox(width: 10),
            const Icon(
              Icons.arrow_drop_down,
              color: Colors.black,
            ),
          ],
        ),
      ),
    );
  }

  Widget _reportTable() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      margin: const EdgeInsets.symmetric(horizontal: 10),
      height: MediaQuery.of(context).size.height * 0.71,
      child: ListView(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Text(
                "Relátório do mês",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const Divider(
                color: Colors.black,
                thickness: 1,
              ),
              _reportItem("Vacas totais", report!.cows.toString()),
              const Divider(
                color: Colors.black,
                thickness: 1,
              ),
              _reportItem("Repetições", report!.repetitions.toString()),
              const Divider(
                color: Colors.black,
                thickness: 1,
              ),
              _reportItem("Crias perdidas", report!.missed_fertilizations.toString()),
              const Divider(
                color: Colors.black,
                thickness: 1,
              ),
              _reportItem("Inseminações", report!.inseminations.toString()),
              const Divider(
                color: Colors.black,
                thickness: 1,
              ),
              _reportItem("Em gestação", report!.pregnants.toString()),
              const Divider(
                color: Colors.black,
                thickness: 1,
              ),
              _reportItem("Secas", report!.cow_drys.toString()),
              const Divider(
                color: Colors.black,
                thickness: 1,
              ),
              _reportItem("Em lactação", report!.cow_lactations.toString()),
              const Divider(
                color: Colors.black,
                thickness: 1,
              ),
              _reportItem("Na engorda", report!.cow_fattening.toString()),
              const Divider(
                color: Colors.black,
                thickness: 1,
              ),
              if (report!.used_semis.isNotEmpty)
                ...[
                  const SizedBox(height: 40),
                  const Text(
                    "Sêmens utilizados",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  for (final usedSemi in report!.used_semis)
                    ...[
                      const SizedBox(height: 20),
                      Row(
                        children: [
                          Text(
                            "${usedSemi.group_name} :",
                            style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          const Spacer(),
                          Text(
                            usedSemi.semis.length.toString(),
                            style: const TextStyle(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      for (final semi in usedSemi.semis)
                        ...[
                          const Divider(
                            color: Colors.black,
                            thickness: 1,
                          ),
                          _reportItem(
                            "Touro: ${semi.bull_id}",
                            "Data de uso: ${dateFormat(semi.used_datetime!)}",
                          ),
                        ],
                      const Divider(
                        color: Colors.black,
                        thickness: 1,
                      ),
                    ],
                ],
            ],
          )
        ],
      )
    );
  }

  Widget _reportItem(String title, String value) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            title,
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            value,
            style: const TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
    );
  }





  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      body: RefreshIndicator(
        onRefresh: getReports,
        child: (){
          if (loading) {
            return Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 10),
                    Text('Carregando as informações...'),
                  ]),
            );
          }else if (error) {
            return UnexpectedError(onRefresh: getReports);
          }else if (report == null) {
            return SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(height: 10),
                    const Text('Relatórios', style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 30),),
                    const SizedBox(height: 10),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        datePicker(),
                      ],
                    ),
                    const SizedBox(height: 10),
                    const Divider(color: secondary, height: 2,),
                    SizedBox(height: height/2 - height/4),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const <Widget>[
                        Icon(
                          Icons.warning_rounded,
                          color: Colors.orange,
                          size: 50,
                        ),
                        SizedBox(height: 20),
                        Text('Nenhum relatório nesta data',
                            style: TextStyle(fontSize: 20, color: Colors
                                .black)),
                      ],
                    ),
                    SizedBox(height: height/2 - height/10),
                  ]
              ),
            );
          }else{
            return SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    const SizedBox(height: 10),
                    const Text('Relatórios', style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 30),),
                    const SizedBox(height: 10),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        datePicker(),
                      ],
                    ),
                    const SizedBox(height: 10),
                    const Divider(color: secondary, height: 2,),
                    _reportTable(),
                  ]
              ),
            );
          }
        }(),
      )
    );
  }

}