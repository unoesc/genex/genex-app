import 'package:inbov/pages/geral_page.dart';
import 'package:inbov/pages/profile_page.dart';
import 'package:inbov/widgets/confirm_dialog.dart';
import 'package:inbov/widgets/generic_loading_dialog.dart';
import 'package:inbov/widgets/response_confirmation.dart';
import '../configs.dart';
import 'package:flutter/material.dart';
// ignore: library_prefixes
import '../http_requests/http_area.dart' as dioArea;
import 'package:inbov/models/area_model.dart';
import '../widgets/unexpected_error.dart';

class AreasPage extends StatefulWidget {
  const AreasPage({Key? key}) : super(key: key);
  @override
  State<AreasPage> createState() => _AreasPageState();
}

class _AreasPageState extends State<AreasPage> {
  bool loading = false;
  bool error = false;

  List<AreaGet> areas = [];

  @override
  void initState() {
    super.initState();
    getAreas();
  }


  Future<void> getAreas() async{
    setState(() {
      loading = true;
      error = false;
    });
    try{
      List<AreaGet> areas = await dioArea.getAreas();
      setState(() {
        this.areas = areas;
      });
    }catch(e, stack){
      print(e);
      print(stack);
      setState(() {
        error = true;
      });
    }finally{
      setState(() {
        loading = false;
      });
    }
  }


  Widget areaCard(AreaGet area){
    return GestureDetector(
      onTap: () async {
        await Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => GeralPage(area: area),
          ),
        ).then((value) => getAreas());
      },
      onLongPress: () async {
        if (await ConfirmDialog.show(context, 'Deseja excluir a fazenda ${area.name}?',)) {
          setState(() {
            loading = true;
          });
          LoadingDialog.show(context, message: 'Excluindo fazenda ${area.name}');
          try{
            await dioArea.deleteArea(area.id);
            //ignore: use_build_context_synchronously
            Navigator.pop(context);
          }catch(e, stack){
            //ignore: use_build_context_synchronously
            Navigator.pop(context);
            print(e);
            print(stack);
            ResponseConfirmation.show(context, 'Erro ao excluir fazenda ${area.name}', 500);
          }finally{
            setState(() {
              loading = false;
            });
          }
          await getAreas();
        }
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 10),
        width: 300,
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.green.shade800,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 10,
              offset: Offset(0, 10),
            ),
          ],
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const Expanded(
                child: SizedBox(width: 20,)
            ),
            Expanded(
              flex: 7,
              child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      area.name,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    area.description != null
                    ? Text(
                      area.description!,
                      style: const TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    )
                    : const SizedBox(height: 20,)
                  ]
              ),
              ),
            const Expanded(
                flex: 1,
                child: Icon(
                    Icons.arrow_forward_ios_rounded,
                    color: Colors.white,
                )
            ),
          ]
        ),
      )
    );
  }


  Widget addNewAreaIcon(){
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30),
        border: Border.all(
          color: Colors.black,
          width: 0.5,
        ),
        boxShadow: const [
          BoxShadow(
            color: Colors.black12,
            blurRadius: 10,
            offset: Offset(0, 10),
          ),
        ],
      ),
      child: IconButton(
        onPressed: () async {
          await Navigator.pushNamed(context, '/add-area').then((value) => getAreas());
        },
        icon: const Icon(
          Icons.add,
          size: 30,
          color: Colors.black,
        ),
      )
    );
  }


  Widget exitIcon() {
    return IconButton(
        icon: const Icon(
          Icons.arrow_back_ios,
        ),
        onPressed: () async {
          if (await ConfirmDialog.show(context, "Deseja realmente sair?", trueButtonText: "Sair", falseButtonText: "Cancelar")) {
            await api.logout();
            // ignore: use_build_context_synchronously
            await Navigator.of(context).pushNamedAndRemoveUntil('/login-area', (Route route) => false);
          }
        },
    );
  }


  Widget configIcon() {
    return IconButton(
        icon: const Icon(
          Icons.person_outline_outlined,
        ),
        onPressed: () async {
          await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const ProfilePage(),
            ),
          ).then((value) => getAreas());
        },
    );
  }



  PreferredSizeWidget appBar(){
    return AppBar(
      title: Row(
        children: [
          exitIcon(),
          const Spacer(),
          const Center(
            child: Text(
              "Fazendas",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const Spacer(),
          configIcon(),
        ],
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: appBar(),
      body:
        RefreshIndicator(
          onRefresh: getAreas,
          child: (){
              if (loading) {
                return Center(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const <Widget>[
                        CircularProgressIndicator(),
                        SizedBox(height: 10),
                        Text('Carregando as informações...'),
                      ]),
                );
              }
              else if (error) {
                return UnexpectedError(onRefresh: getAreas);
              }
              else if (areas.isEmpty) {
                return Center(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: const <Widget>[
                         Icon(
                          Icons.error_outline,
                          color: Colors.red,
                          size: 50,
                        ),
                        SizedBox(height: 20),
                        Text('Nenhuma fazenda cadastrada', style: TextStyle(fontSize: 20, color: Colors.black)),
                      ]
                  ),
                );
              }
              else{
                return SingleChildScrollView(
                  child:
                    Center(
                      child:
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            const SizedBox(height: 20),
                            const Text('Selecione sua fazenda:', style: TextStyle(fontSize: 20, color: Colors.black), textAlign: TextAlign.center,),
                            const Divider(color: secondary,),
                            Container(
                              height: height - 135,
                              alignment: Alignment.center,
                              width: width - 100,
                              child: ListView(
                                  shrinkWrap: true,
                                  scrollDirection: Axis.vertical,
                                  children: <Widget>[
                                    for (AreaGet area in areas)
                                      areaCard(area),
                                  ]
                              ),
                            )
                          ]
                        )
                    )
                );
              }
           }(),
        ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: addNewAreaIcon(),
    );
  }
}