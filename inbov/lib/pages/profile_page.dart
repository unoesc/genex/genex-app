import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:inbov/models/user_model.dart';
import 'package:inbov/widgets/generic_loading_dialog.dart';
import '../configs.dart';
import '../utils/formatters.dart';
import '../utils/validators.dart';
import '../widgets/confirm_dialog.dart';
import '../widgets/text_field.dart';
import 'package:flutter/material.dart';
// ignore: library_prefixes
import '../http_requests/http_user.dart' as dioUser;
import '../widgets/response_confirmation.dart';
import '../widgets/unexpected_error.dart';


class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);
  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  MaskedTextController phoneController = MaskedTextController(
    mask: '(00) 0000-0000',
    text: '0000000000',
  );

  bool loading = false;
  bool error = false;
  bool requestLoading = false;

  late GetUser user;

  @override
  void initState() {
    super.initState();
    getUserData();
  }

  bool verifyOnlyNumbers(String value) {
    RegExp regExp = RegExp(r'^[0-9]+$');
    return regExp.hasMatch(value);
  }

  Future<void> setUserData() async {
    setState(() {
      nameController.text = user.name;
      emailController.text = user.email;
      if (user.phone != null && verifyOnlyNumbers(user.phone!)) {
        if (user.phone!.length > 14) {
          phoneController.updateMask('(00) 00000-0000');
        }
        phoneController.text = user.phone!;
      }
    });
  }

  Future<bool> isValid() async {
    bool valid = false;
    if (_formKey.currentState!.validate() && !loading && !requestLoading) {
      valid = await ConfirmDialog.show(context, "Deseja salvar as alterações?");
    }
    return valid;
  }

  Future<void> saveFunction() async {
    setState(() {
      requestLoading = true;
    });
    LoadingDialog.show(context);
    try {
      Map<String, dynamic> data = {
        "name": nameController.text,
        "email": emailController.text,
        "phone": removeChars(phoneController.text),
      };
      await dioUser.updateUser(data);
      //ignore: use_build_context_synchronously
      Navigator.pop(context);
      //ignore: use_build_context_synchronously
      await ResponseConfirmation.show(context, "Dados alterados com sucesso", 200);
      await getUserData();
    } catch (e) {
      Navigator.pop(context);
      if (e == 409) {
        ResponseConfirmation.show(
            context, "O E-mail informado já está em uso", 409);
      } else {
        ResponseConfirmation.show(context,
            "Ocorreu um erro interno, por favor tente mais tarde", 500);
      }
    } finally {
      setState(() {
        requestLoading = false;
      });
    }
  }


  Future<void> getUserData() async {
    setState(() {
      loading = true;
      error = false;
    });
    try {
      GetUser user = await dioUser.getUser();
      setState(() {
        this.user = user;
      });
      await setUserData();
    }catch(e, stack){
      print(e);
      print(stack);
      setState(() {
        error = true;
      });
    }finally{
      setState(() {
        loading = false;
      });
    }
  }

  Widget _dataUser(String type, String title, TextEditingController controller,
      TextInputType key) {
    return TextFieldPage(
      focus: false,
      outlined: true,
      capitalization: type == 'name'
          ? TextCapitalization.sentences
          : TextCapitalization.none,
      controller: controller,
      loading: loading,
      obscure: false,
      text: title,
      typeInput: key,
      typeHints: type == 'email'
          ? [AutofillHints.email]
          : type == 'phone'
          ? [AutofillHints.telephoneNumber]
          : [AutofillHints.name],
      hintText: type == 'phone' ? "(##) #####-####" : null,
      validate: type == 'name'
          ? requiredFieldValidator
          : type == "email"
          ? requiredFieldValidator
          : type == 'phone'
          ? requiredFieldValidator
          : null,
    );
  }

  Widget _alterDataProfileButton() {
    return ElevatedButton(
      onPressed: () async {
        if (await isValid()) {
          await saveFunction();
        }
      },
      style: ElevatedButton.styleFrom(
        primary: primary,
        padding: const EdgeInsets.symmetric(vertical: 13),
      ),
      child: const Text(
        'SALVAR',
        style: TextStyle(color: Colors.white),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Perfil'),
      ),
      body: RefreshIndicator(
        onRefresh: getUserData,
        child: (){
          if (loading) {
            return Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 10),
                    Text('Carregando as informações...'),
                  ]),
            );
          }else if (error) {
            return UnexpectedError(
              onRefresh: getUserData,
            );
          }else{
            return SingleChildScrollView(
              child: Container(
                height: height,
                padding: const EdgeInsets.all(10),
                child:
                  Column(
                    children: <Widget>[
                      AutofillGroup(
                          child: Form(
                            autovalidateMode: AutovalidateMode.onUserInteraction,
                            key: _formKey,
                            child: Column(
                              children: <Widget>[
                                const SizedBox(height: 20),
                                _dataUser('name', 'Nome', nameController, TextInputType.text),
                                const SizedBox(height: 10),
                                _dataUser('email', 'E-mail', emailController, TextInputType.emailAddress),
                                const SizedBox(height: 10),
                                _dataUser('phone', 'Telefone', phoneController, TextInputType.phone),
                                const SizedBox(height: 30),
                                _alterDataProfileButton(),
                              ],
                            ),
                          )
                      )
                    ]
                  )
              )
            );
          }
        }(),
      )
    );
  }


}