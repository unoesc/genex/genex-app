import 'package:flutter/material.dart';
import 'package:inbov/models/area_model.dart';
import 'package:inbov/pages/add_area_page.dart';
import 'package:inbov/pages/sub-pages/animal_page.dart';
import 'package:inbov/pages/sub-pages/events_page.dart';
import 'package:inbov/pages/sub-pages/report_page.dart';
import 'package:inbov/pages/sub-pages/semis_page.dart';

import '../configs.dart';

class GeralPage extends StatefulWidget {
  final AreaGet area;
  final int? index;
  const GeralPage(
      {
        Key? key,
        required this.area,
        this.index,
      }
      ) : super(key: key);
  @override
  State<StatefulWidget> createState() => _GeralPageState();
}

class _GeralPageState extends State<GeralPage> {

  int selectedIndex = 0;


  @override
  void initState() {
    super.initState();
    if(widget.index != null){
      selectedIndex = widget.index!;
    }
  }


  Widget bottomNavigatorBar(){
    return BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Image.asset('assets/cow_96.png', width: 20, height: 20,),
            label: "Animais",
          ),
          const BottomNavigationBarItem(
            icon: Icon(Icons.event, color: Colors.grey,),
            label: "Eventos",
          ),
          const BottomNavigationBarItem(
              icon: Icon(Icons.list_alt, color: Colors.grey,),
              label: "Relatórios"
          ),
          if (widget.area.gene_activated)
            BottomNavigationBarItem(
              icon: Image.asset('assets/semi_96.png', width: 20, height: 20,),
              label: "Sêmens",
            ),
        ],
      currentIndex: selectedIndex,
      unselectedItemColor: Colors.black,
      selectedItemColor: primary,
      onTap: (index) {
        setState(() {
          selectedIndex = index;
        });
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.area.name),
        actions: [
          IconButton(
            icon: const Icon(Icons.map),
            onPressed: () async {
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AddAreaPage(area: widget.area,),
                ),
              ).then((value) => Navigator.pop(context));
            },
          ),
        ],
      ),
      body: Scaffold(
        resizeToAvoidBottomInset: false,
        body: (){
          switch(selectedIndex){
            case 0:
              return AnimalPage(area: widget.area);
            case 1:
              return EventsPage(area: widget.area);
            case 2:
              return ReportPage(area: widget.area);
            case 3:
              return SemisPage(area: widget.area);
          }
        }(),
      ),
      bottomNavigationBar: bottomNavigatorBar(),
    );
  }

}