import 'package:extended_masked_text/extended_masked_text.dart';
import 'package:inbov/utils/formatters.dart';
// ignore: library_prefixes
import '../http_requests/http_user.dart' as dioUser;
import '../widgets/response_confirmation.dart';
import '../widgets/text_field.dart';
import 'package:flutter/material.dart';
import 'package:inbov/widgets/generic_loading_dialog.dart';
import '../configs.dart';
import '../utils/validators.dart';
import '../widgets/password_field.dart';


class SignupPage extends StatefulWidget {
  const SignupPage({Key? key}) : super(key: key);
  @override
  State<SignupPage> createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  TextEditingController emailController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  MaskedTextController phoneController = MaskedTextController(mask: '(00) 0000-0000');
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  bool loading = false;



  Widget _entryField() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 20),
        TextFieldPage(
          capitalization: TextCapitalization.sentences,
          loading: loading,
          controller: nameController,
          obscure: false,
          text: 'Nome Completo *',
          validate: requiredFieldValidator,
          outlined: true,
          focus: false,
        ),
      ],
    );
  }

  Widget _entryFieldEmail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 20),
        TextFieldPage(
          controller: emailController,
          loading: loading,
          obscure: false,
          text: 'E-mail *',
          typeInput: TextInputType.emailAddress,
          typeHints: const [AutofillHints.email],
          validate: requiredFieldValidator,
          focus: false,
          outlined: true,
        ),
      ],
    );
  }


  Widget _entryFieldPassword() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 20),
        PasswordField(
          signUp: false,
          controller: passwordController,
          loading: loading,
          text: 'Senha *',
          validate: requiredFieldValidator,
        ),
        const SizedBox(height: 10),
        PasswordField(
          signUp: false,
          loading: loading,
          controller: confirmPasswordController,
          text: 'Confirmação de Senha *',
          validationController: passwordController,
        ),
      ],
    );
  }


  Widget _entryFieldPhone() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 20),
        TextFieldPage(
          controller: phoneController,
          loading: loading,
          obscure: false,
          text: 'Telefone',
          typeInput: TextInputType.phone,
          validate: null,
          focus: false,
          outlined: true,
        ),
      ],
    );
  }


  Widget _registerUser() {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        children: <Widget>[
          _entryField(),
          _entryFieldEmail(),
          _entryFieldPhone(),
          const SizedBox(height: 20),
          const Text('Crie uma senha para acessar o aplicativo',
              style: TextStyle(
                fontSize: 16,
                color: secondary,
              )),
          const SizedBox(height: 10),
          _entryFieldPassword(),
        ],
      ),
    );
  }


  Widget _submitButton() {
    return InkWell(
      onTap: () async {
        if(_formKey.currentState!.validate() && !loading){
          setState(() {
            loading = true;
          });
          LoadingDialog.show(context, message: 'Cadastrando usuário...');
          try{
            Map<String, dynamic> data = {
              'name': nameController.text,
              'email': emailController.text,
              'phone': removeChars(phoneController.text),
              'password': passwordController.text,
            };
            await dioUser.createUser(data);
            setState(() {
              nameController.clear();
              emailController.clear();
              phoneController.clear();
              passwordController.clear();
              confirmPasswordController.clear();
            });
            //ignore: use_build_context_synchronously
            Navigator.pop(context);
            //ignore: use_build_context_synchronously
            Navigator.pushNamedAndRemoveUntil(context, '/area-area', (_) => false);
          }catch(e, stack)
          {
            Navigator.pop(context);
            print(e);
            print(stack);
            if (e == 409) {
              ResponseConfirmation.show(context, "Email informado ja cadastrado", 409);
            }
            else{
              ResponseConfirmation.show(context, "Ocorreu um erro interno, tente mais tarde", 500);
            }
          }finally{
            setState(() {
              loading = false;
            });
          }
        }
      },
      child: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          gradient: const LinearGradient(
            colors: [
              Color(0xFF4caf50),
              Color(0xFF8bc34a),
            ],
          ),
        ),
        child: const Text(
          'Cadastrar',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Cadastro',
          style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold
          ),
        ),
      ),
      body: SizedBox(
        height: height,
        child: SingleChildScrollView(
          child: Center(
            child: Container(
              padding: const EdgeInsets.only(
                  top: 10, bottom: 30, left: 20, right: 20),
              child: Column(
                children: [
                  const SizedBox(height: 20),
                  AutofillGroup(
                      child: Form(
                        key: _formKey,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        child: _registerUser(),
                      ),
                  ),
                  const SizedBox(height: 20),
                  _submitButton(),
                  const SizedBox(height: 20),
                ],
              )
            )
          ),
        )
      ),
    );
  }

}